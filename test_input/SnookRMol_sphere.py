#!/usr/bin/env python

"""
This program provides a command line interface on top of the core classes
of the  SnookRMol package. The input file in toml format define the 
calculations that have to be implemented and the required methodology.
"""

import sys
import os
import argparse
from typing import Union
from pathlib import Path

# tomllib will be use in standard library from python 3.11
try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

sys.path.append("../src/")

_ = r"""
  ____                    _    ____  __  __       _ 
 / ___| _ __   ___   ___ | | _|  _ \|  \/  | ___ | |
 \___ \| '_ \ / _ \ / _ \| |/ / |_) | |\/| |/ _ \| |
  ___) | | | | (_) | (_) |   <|  _ <| |  | | (_) | |
 |____/|_| |_|\___/ \___/|_|\_\_| \_\_|  |_|\___/|_|
                                                    
"""

TITLE = r"""
  ______                   _     ______  _______       _  
 / _____)                 | |   (_____ \(_______)     | | 
( (____  ____   ___   ___ | |  _ _____) )_  _  _  ___ | | 
 \____ \|  _ \ / _ \ / _ \| |_/ )  __  /| ||_|| |/ _ \| | 
 _____) ) | | | |_| | |_| |  _ (| |  \ \| |   | | |_| | | 
(______/|_| |_|\___/ \___/|_| \_)_|   |_|_|   |_|\___/ \_)

                                                    H
                                                   /
c========-------------------------o  *OH       H--O    
                               
"""


def exist(filename: Union[str, Path]):
    """ 'Type' for argparse - checks that file exists but does not open """
    if not os.path.isfile(filename):
        raise argparse.ArgumentTypeError(
            "Input file %s does not exist" % filename)
    return filename

def get_options() -> dict:
    """ get options from the command line """

    parser = argparse.ArgumentParser(
        prog="build",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
    )

    parser.add_argument("-i", "--input",
                        help="SnookRMol input file",
                        metavar="INPUT.toml",
                        default="input.toml",
                        type=exist)

    parser.add_argument("-o", "--out",
                        help="output file name, 'stdout' output to stdout",
                        metavar="OUTPUT",
                        default="snooker.out",
                        type=str)

    return vars(parser.parse_args())


# class Formatter(logging.Formatter):
#     """ This class defines different format depending on logging level """

#     def format(self, record):
#         if record.levelno == logging.INFO:
#             self._style._fmt = "%(message)s"
#         else:
#             self._style._fmt = "%(levelname)s: %(message)s"
#         return super().format(record)

def play_snooker():
    pass

def main():
    """ Main function to run the build system of data analyzes """

    # read command line arguments
    # ---------------------------
    args = get_options()
    print(TITLE)

    # define main logger
    # ------------------
    # logger = logging.getLogger("main")

    # define logging to file
    # logging.basicConfig(
    #     level=logging.DEBUG,
    #     format="%(levelname)-8s: %(message)s",
    #     filename=args["out"],
    #     filemode="w",
    # )

    # INFO message and higher on the console
    # console = logging.StreamHandler()
    # console.setLevel(logging.INFO)
    # console.setFormatter(Formatter())
    # logger.addHandler(console)

    # Start program
    # -------------
    # datefmt = "%b %d, %Y at %H:%M:%S"
    # logger.info('Build started at %s', datetime.now().strftime(datefmt))
    # logger.info("EMVOL project\n")
    # logger.info(TITLE)

    # read input file which define experiments
    inp = Path(args["input"])
    # logger.info("Readding input file: %s", inp)
    with inp.open("rb") as fp:
        controls = tomllib.load(fp)

    # run main
    play_snooker(controls)
    # logger.info("-" * 72)
    # logger.info("Finished at %s", datetime.now().strftime(datefmt))


if __name__ == "__main__":
    sys.exit(main())





