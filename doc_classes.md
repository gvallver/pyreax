# PyReax classes



```mermaid
classDiagram
    direction TB
    class Atom
    Atom : itype
    Atom : mass
    Atom : element
    Atom : charge
    class ReaxDataFile{
        structure
        atoms
        atom_types
        title
        export_lammps_data()
        from_file()
        _parse_data_file()
    }
    class ReaxBO{
        elements
        atom_types
        n_atoms
        atom_data
        bond_data
        connectivity
        _parse()
        _read_atom_types()
    }
    class ReaxParams{
        blocks
        blocks_els
        params_keys
        get_param_ids()
        get_param_keys()
        get_params()
    }
    class ReaxSpecies{
        data_mol
        data 
        molecules
        get_proportions()
    }
```   