# coding: utf-8

from enum import Enum, unique
from typing import Union
import numpy as np

__all__ = ["get_random_grid", "get_random_grid_inv", "get_regular_grid",
           "get_regular_grid_inv", "SphericGrid", "FusedSpheresGrid",
           "FusedSpheresGrid_Randoms", "GridType", "GridShape"]

Vector3N = list[list[float, float, float]]


@unique
class GridType(Enum):
    regular = "regular"
    regular_inv = "regular_inv"
    random = "random"
    random_inv = "random_inv"


@unique
class GridShape(Enum):
    sphere = "sphere"
    fused_sphere = "fused_sphere"


def get_random_grid(radius: float = 1.0, n_points: int = 10,
                    thetalim: tuple[float, float] = (0, 180),
                    philim: tuple[float, float] = (0, 360),
                    radians: bool = False) -> np.ndarray:
    """ Return random points equi-distributed on a sphere.

    Args:
        radius (float): sphere radius, default 1.
        n_points (int): number of points to be sampled, default 10.
        thetalim (float, float): min and max value of theta to limit the
            sampling to a slice of the surface of the sphere.
        philim (float, float): min and max value of phi to limit the
            sampling to a slice of the surface of the sphere.
        radians (bool): if True, thetalim and philim have to be set in
            radians, default is False and values are in degrees.

    Returns:
        Returns a numpy array of shape (n_points, 3) containing the
        sampled points.
    """

    # convert in radians
    thetalim = np.radians(thetalim) if not radians else thetalim
    philim = np.radians(philim) if not radians else philim

    # boundaries over theta, compute z boundaries
    zmin = radius * np.cos(thetalim[0])
    zmax = radius * np.cos(thetalim[1])
    zmin, zmax = np.sort([zmin, zmax])
    z = np.random.uniform(zmin, zmax, n_points)

    # compute phi values
    phi = np.random.uniform(philim[0], philim[1], n_points)

    # compute the grid
    x = np.sqrt(radius**2 - z**2) * np.cos(phi)
    y = np.sqrt(radius**2 - z**2) * np.sin(phi)

    return np.array([x, y, z], dtype=np.float64).transpose()


def get_random_grid_inv(radius: float = 1.0, n_points: int = 10,
                        thetalim: tuple[float, float] = (0, 180),
                        philim: tuple[float, float] = (0, 360),
                        radians: bool = False) -> np.ndarray:
    """ Return random points equi-distributed on a sphere.

    Args:
        radius (float): sphere radius, default 1.
        n_points (int): number of points to be sampled, default 10.
        thetalim (float, float): min and max value of theta to limit the
            sampling to a slice of the surface of the sphere.
        philim (float, float): min and max value of phi to limit the
            sampling to a slice of the surface of the sphere.
        radians (bool): if True, thetalim and philim have to be set in
            radians, default is False and values are in degrees.

    Returns:
        Returns a numpy array of shape (n_points, 3) containing the
        sampled points.
    """

    # convert in radians
    thetalim = np.radians(thetalim) if not radians else thetalim
    philim = np.radians(philim) if not radians else philim

    # boundaries over theta, compute z boundaries
    zmin = radius * np.cos(thetalim[0])
    zmax = radius * np.cos(thetalim[1])
    zmin, zmax = np.sort([zmin, zmax])
    z = np.random.uniform(zmin, zmax, n_points)

    # compute phi values
    phi = np.random.uniform(philim[0], philim[1], n_points)

    # compute the grid
    x = np.sqrt(radius**2 - z**2) * np.cos(phi)
    y = np.sqrt(radius**2 - z**2) * np.sin(phi)

    return (np.array([x, y, z], dtype=np.float64).transpose(),
            np.array([-x, -y, -z], dtype=np.float64).transpose())


def get_regular_grid(radius: float = 1.0, n_points: int = 10,
                     thetalim: tuple[float, float] = (0., 180.),
                     philim: tuple[float, float] = (0., 360.),
                     radians: bool = False) -> np.ndarray:
    """ Returns a regular grid at the surface of the sphere

    Args:
        radius (float): sphere radius, default 1.
        n_points (int): number of points to be sampled, default 10.
        thetalim (float, float): min and max value of theta to limit the
            sampling to a slice of the surface of the sphere.
        philim (float, float): min and max value of phi to limit the
            sampling to a slice of the surface of the sphere.
        radians (bool): if True, thetalim and philim have to be set in
            radians, default is False and values are in degrees.

    Returns:
        Returns a numpy array of shape (N, 3) containing the
        sampled points. N would be close to n_points but is not exactly
        the same.
    """

    # sort and convert in radians
    if not radians:
        thetalim = np.sort(np.radians(thetalim))
        philim = np.sort(np.radians(philim))
    else:
        thetalim = np.sort(thetalim)
        philim = np.sort(philim)

    # boundaries over theta
    theta_int = -np.cos(thetalim[1]) + np.cos(thetalim[0])
    # boundaries over phi
    phi_int = philim[1] - philim[0]
    # area of one point
    area = theta_int * phi_int / n_points
    distance = np.sqrt(area)

    # theta values
    N_theta = int((thetalim[1] - thetalim[0]) / distance)
    step_theta = (thetalim[1] - thetalim[0]) / N_theta
    step_phi = area / step_theta

    # compute the grid
    x = list()
    y = list()
    z = list()

    for i in range(N_theta):
        theta = thetalim[0] + (i + 0.5) * step_theta
        N_phi = int((philim[1] - philim[0]) * np.sin(theta) / step_phi)
        for j in range(N_phi):
            phi = philim[0] + (philim[1] - philim[0]) * j / N_phi
            x.append(radius * np.sin(theta) * np.cos(phi))
            y.append(radius * np.sin(theta) * np.sin(phi))
            z.append(radius * np.cos(theta))

    return np.array([x, y, z], dtype=np.float64).transpose()


def get_regular_grid_inv(radius: float = 1.0, n_points: int = 10,
                         thetalim: tuple[float, float] = (0., 180.),
                         philim: tuple[float, float] = (0., 360.),
                         radians: bool = False) -> np.ndarray:
    """ Returns a regular grid at the surface of the sphere

    Args:
        radius (float): sphere radius, default 1.
        n_points (int): number of points to be sampled, default 10.
        thetalim (float, float): min and max value of theta to limit the
            sampling to a slice of the surface of the sphere.
        philim (float, float): min and max value of phi to limit the
            sampling to a slice of the surface of the sphere.
        radians (bool): if True, thetalim and philim have to be set in
            radians, default is False and values are in degrees.

    Returns:
        Returns a numpy array of shape (N, 3) containing the
        sampled points. N would be close to n_points but is not exactly
        the same.
    """

    # sort and convert in radians
    if not radians:
        thetalim = np.sort(np.radians(thetalim))
        philim = np.sort(np.radians(philim))
    else:
        thetalim = np.sort(thetalim)
        philim = np.sort(philim)

    # boundaries over theta
    theta_int = -np.cos(thetalim[1]) + np.cos(thetalim[0])
    # boundaries over phi
    phi_int = philim[1] - philim[0]
    # area of one point
    area = theta_int * phi_int / n_points
    distance = np.sqrt(area)

    # theta values
    N_theta = int((thetalim[1] - thetalim[0]) / distance)
    step_theta = (thetalim[1] - thetalim[0]) / N_theta
    step_phi = area / step_theta

    # compute the grid

    xv = list()
    yv = list()
    zv = list()
    for i in range(N_theta):
        theta = thetalim[0] + (i + 0.5) * step_theta
        N_phi = int((philim[1] - philim[0]) * np.sin(theta) / step_phi)
        for j in range(N_phi):
            phi = philim[0] + (philim[1] - philim[0]) * j / N_phi
            xv.append(-radius * np.sin(theta) * np.cos(phi))
            yv.append(-radius * np.sin(theta) * np.sin(phi))
            zv.append(-radius * np.cos(theta))

    return np.array([xv, yv, zv], dtype=np.float64).transpose()


class SphericGrid:
    """ This class represent a points grid at the surface of a sphere """

    def __init__(self, radius: float = 1., n_points: int = 10,
                 thetalim: tuple[float, float] = (0., 180.),
                 philim: tuple[float, float] = (0., 360.),
                 radians: bool = False,
                 grid_type: str = GridType.regular):
        """ Initialize all parameters of the grid

        Args:
            radius (float): sphere radius, default 1.
            n_points (int): number of points to be sampled, default 10.
            thetalim (float, float): min and max value of theta to limit the
                sampling to a slice of the surface of the sphere.
            philim (float, float): min and max value of phi to limit the
                sampling to a slice of the surface of the sphere.
            radians (bool): if True, thetalim and philim have to be set in
                radians, default is False and values are in degrees.

        """
        self.n_points = n_points
        self.radius = radius
        self.thetalim = thetalim
        self.philim = philim
        self.radians = radians
        self.grid_type = GridType(grid_type)
        self._grid = self._setup_grid()

    @property
    def grid(self):
        """ The grid at the surface of the sphere """
        return self._grid

    @property
    def unpack(self):
        """ Return the grid with a shape (3, N) ready to unpack """
        return self._grid.transpose()

    @property
    def grid_size(self):
        """ Return the true number of points which can be different from the
        one in input in case of regular grid. """
        return self._grid.shape[0]

    def export_grid(self, filename: str = "points.xyz"):
        """ Export the grid points in a xyz format """

        grid = self.grid.transpose()
        header = f"{len(grid)}\ngrid of initial points\n"
        np.savetxt(filename, grid, fmt="%12.6f", header=header)

    def _setup_grid(self):
        """ return the grid """
        if self.grid_type == GridType.regular:
            return get_regular_grid(
                self.radius, self.n_points, self.thetalim, self.philim,
                self.radians)
        elif self.grid_type == GridType.regular_inv:
            return get_regular_grid_inv(
                self.radius, self.n_points, self.thetalim, self.philim,
                self.radians)
        elif self.grid_type == GridType.random:
            return get_random_grid(
                self.radius, self.n_points, self.thetalim, self.philim,
                self.radians)


class FusedSpheresGrid:
    """ This class represents a fused sphere grid. Points which are located
    inside a neighbor sphere are removed. """

    def __init__(self, centers: Union[list[float], Vector3N],
                 radius: Union[float, list[float]] = 1.,
                 n_points: Union[int, list[int]] = 10,
                 thetalim: tuple[float, float] = (0., 180.),
                 philim: tuple[float, float] = (0., 360.),
                 radians: bool = False,
                 grid_type: str = GridType.regular):
        """ Set up the grid from the coordinates of the center of the spheres,
        the radius of each sphere and the number of points for each sphere.

        Args:
            centers (array, 2D): Coordinates in R^3 of the centers of the
                spheres
            radius (list, float): list of radius for each sphere. If only
                one value is given, it is used for all spheres.
            n_points (list, int): number of points on each sphere. If only
                one value is give, it is used for all spheres.
            radius (float): sphere radius, default 1.
            n_points (int): number of points to be sampled, default 10.
            thetalim (float, float): min and max value of theta to limit the
                sampling to a slice of the surface of the sphere.
            philim (float, float): min and max value of phi to limit the
                sampling to a slice of the surface of the sphere.
            radians (bool): if True, thetalim and philim have to be set in
                radians, default is False and values are in degrees.

        """

        self.thetalim = thetalim
        self.philim = philim
        self.radians = radians
        self.grid_type = GridType(grid_type)

        # check centers
        try:
            self.centers = np.array(centers, dtype=np.float64)
            self.centers = self.centers.reshape(self.centers.size // 3, 3)
        except ValueError as error:
            print("centers = ", centers)
            raise ValueError("Cannot convert centers in a ndarray of floats"
                             " with a shape (N, 3).") from error

        # set radius for all spheres
        try:
            _ = len(radius)
        except TypeError:
            radius = [radius] * len(self.centers)

        try:
            self.radius = np.array(radius, dtype=np.float64)
        except ValueError as error:
            raise ValueError("Cannot convert radius in a ndarray of floats: "
                             f"{radius}.") from error

        # set number of points for all spheres
        try:
            _ = len(n_points)
        except TypeError:
            n_points = [n_points] * len(centers)

        try:
            self.n_points = np.array(n_points, dtype=np.int32)
        except ValueError as error:
            raise ValueError("Cannot convert n_points in a ndarray of int: "
                             f"{n_points}.") from error

        # check centers and radius have the same lengths
        if len(set([len(centers), len(radius), len(n_points)])) != 1:
            raise ValueError(
                f"Number of radius, centers and n_points inconsistent:\n"
                f"len(radius) = {len(self.radius)}\n radius {self.radius} "
                f"len(centers) = {len(self.centers)}\n centers {self.centers} "
                f"len(n_points) = {len(self.n_points)}\n n_points {self.n_points}")

        self._grids = self._setup_grids()
        self._grid = np.concatenate(self._grids)

    @property
    def grids(self):
        """ The list of grids at the surface of each spheres """
        return self._grids

    @property
    def grid(self):
        """ Return the concatenation of all spherical grid """
        return self._grid

    def unpack(self):
        """ Return the concatenated grids with a shape (3, N) ready to
        unpack """
        return self.grid.transpose()

    @property
    def grid_size(self):
        """ Return the true number of points which can be different from the
        one in input in case of regular grid. """
        return self.grid.shape[0]

    def export_grid(self, filename: str = "points.xyz"):
        """ Export the grid points in a xyz format """

        grid = self.grid.transpose()
        header = f"{len(grid)}\ngrid of initial points\n"
        np.savetxt(filename, grid, fmt="%12.6f", header=header)

    def _setup_grids(self):
        """ Compute a fused sphere grid by concatenating the grids at the
        surface of all spheres and remove points that are inside another
        sphere.
        """

        grids = list()
        for i in range(len(self.radius)):
            if self.grid_type == GridType.regular_inv:

                sgrid1 = SphericGrid(
                    n_points=self.n_points[i], radius=self.radius[i],
                    thetalim=self.thetalim, philim=self.philim,
                    grid_type=GridType.regular, radians=self.radians
                )

                sgrid2 = SphericGrid(
                    n_points=self.n_points[i], radius=self.radius[i],
                    thetalim=self.thetalim, philim=self.philim,
                    grid_type=self.grid_type, radians=self.radians
                )

                points = sgrid1.grid + self.centers[i]
                points2 = sgrid2.grid + self.centers[i]

                # remove points inside other sphere
                for j in range(len(self.radius)):
                    if i == j:
                        # skip sphere grid corresponding to the current center
                        continue

                    distances = np.sqrt(
                        np.sum((points - self.centers[j])**2, axis=1))
                    idx = np.where(distances >= self.radius[j])[0]
                    points = points[idx]
                    points2 = points2[idx]

                grids.append(points2)

            else:

                sgrid = SphericGrid(
                    n_points=self.n_points[i], radius=self.radius[i],
                    thetalim=self.thetalim, philim=self.philim,
                    grid_type=self.grid_type, radians=self.radians
                )

                points = sgrid.grid + self.centers[i]
                # remove points inside other sphere
                for j in range(len(self.radius)):
                    if i == j:
                        # skip sphere grid corresponding to the current center
                        continue

                    distances = np.sqrt(
                        np.sum((points - self.centers[j])**2, axis=1))
                    idx = np.where(distances >= self.radius[j])[0]
                    points = points[idx]
                    # print(idx)
                grids.append(points)

        return grids


class FusedSpheresGrid_Randoms:
    """ This class represents a fused sphere grid. Points which are located
    inside a neighbor sphere are removed. """

    def __init__(self, centers: Union[list[float], Vector3N],
                 radius: Union[float, list[float]] = 1.,
                 n_points: Union[int, list[int]] = 10,
                 thetalim: tuple[float, float] = (0., 180.),
                 philim: tuple[float, float] = (0., 360.),
                 radians: bool = False,
                 grid_type: str = GridType.random):
        """ Set up the grid from the coordinates of the center of the spheres,
        the radius of each sphere and the number of points for each sphere.

        Args:
            centers (array, 2D): Coordinates in R^3 of the centers of the
                spheres
            radius (list, float): list of radius for each sphere. If only
                one value is given, it is used for all spheres.
            n_points (list, int): number of points on each sphere. If only
                one value is give, it is used for all spheres.
            radius (float): sphere radius, default 1.
            n_points (int): number of points to be sampled, default 10.
            thetalim (float, float): min and max value of theta to limit the
                sampling to a slice of the surface of the sphere.
            philim (float, float): min and max value of phi to limit the
                sampling to a slice of the surface of the sphere.
            radians (bool): if True, thetalim and philim have to be set in
                radians, default is False and values are in degrees.

        """

        self.thetalim = thetalim
        self.philim = philim
        self.radians = radians
        self.grid_type = GridType(grid_type)

        # check centers
        try:
            self.centers = np.array(centers, dtype=np.float64)
            self.centers = self.centers.reshape(self.centers.size // 3, 3)
        except ValueError as error:
            print("centers = ", centers)
            raise ValueError("Cannot convert centers in a ndarray of floats"
                             " with a shape (N, 3).") from error

        # set radius for all spheres
        try:
            _ = len(radius)
        except TypeError:
            radius = [radius] * len(self.centers)

        try:
            self.radius = np.array(radius, dtype=np.float64)
        except ValueError as error:
            raise ValueError("Cannot convert radius in a ndarray of floats: "
                             f"{radius}.") from error

        # set number of points for all spheres
        try:
            _ = len(n_points)
        except TypeError:
            n_points = [n_points] * len(centers)

        try:
            self.n_points = np.array(n_points, dtype=np.int32)
        except ValueError as error:
            raise ValueError("Cannot convert n_points in a ndarray of int: "
                             f"{n_points}.") from error

        # check centers and radius have the same lengths
        if len(set([len(centers), len(radius), len(n_points)])) != 1:
            raise ValueError(
                f"Number of radius, centers and n_points inconsistent:\n"
                f"len(radius) = {len(self.radius)}\n radius {self.radius} "
                f"len(centers) = {len(self.centers)}\n centers {self.centers} "
                f"len(n_points) = {len(self.n_points)}\n n_points {self.n_points}")

        self._grids, self._grids2 = self._setup_grids()
        self._grid = np.concatenate(self._grids)
        self._grid2 = np.concatenate(self._grids2)

    @property
    def grids(self):
        """ The list of grids at the surface of each spheres """
        return self._grids

    @property
    def grids2(self):
        """ The list of grids at the surface of each spheres """
        return self._grids2

    @property
    def grid(self):
        """ Return the concatenation of all spherical grid """
        return self._grid

    @property
    def grid2(self):
        """ Return the concatenation of all spherical grid """
        return self._grid2

    @property
    def unpack(self):
        """ Return the concatenated grids with a shape (3, N) ready to
        unpack """
        return self.grid.transpose()

    @property
    def unpack2(self):
        """ Return the concatenated grids with a shape (3, N) ready to
        unpack """
        return self.grid2.transpose()

    @property
    def grid_size(self):
        """ Return the true number of points which can be different from
        the one in input in case of regular grid. """
        return self.grid.shape[0]

    @property
    def grid2_size(self):
        """ Return the true number of points which can be different from
        the one in input in case of regular grid. """
        return self.grid2.shape[0]

    def _setup_grids(self):
        """ Compute a fused sphere grid by concatenating the grids at the
        surface of all spheres and remove points that are inside another
        sphere.
        """

        grids = list()
        grids2 = list()
        for i in range(len(self.radius)):
            sgrid1 = SphericGrid(
                n_points=self.n_points[i], radius=self.radius[i],
                thetalim=self.thetalim, philim=self.philim,
                grid_type=GridType.random, radians=self.radians
            )
            sgrid2 = -sgrid1.grid
            points = sgrid1.grid + self.centers[i]
            points2 = sgrid2 + self.centers[i]

            # remove points inside other sphere
            for j in range(len(self.radius)):
                if i == j:
                    # skip sphere grid corresponding to the current center
                    continue

                distances = np.sqrt(
                    np.sum((points - self.centers[j])**2, axis=1))
                idx = np.where(distances >= self.radius[j])[0]
                points = points[idx]
                points2 = points2[idx]

            grids.append(points)
            grids2.append(points2)

        return grids, grids2
