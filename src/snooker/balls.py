# encoding: utf-8

""" This module defines the default balls molecules to be used as
projectile. """

from typing import Union
from enum import Enum, unique
from pathlib import Path

from pymatgen.core import Molecule

__all__ = ["Ball", "default_balls", "CustomBall"]


@unique
class Ball(Enum):
    hydroxyl = "OH"
    oxygen = "O2"
    ozone = "O3"
    sulfate = "SO4_2m"
    custom = "custom"

    @property
    def molecule(self):
        if self == "custom":
            return None
        else:
            return default_balls[self]


# read all xyz files and store Molecule object in a dict
BALLS_PATH = Path(__file__).parent / "ball_molecules"
default_balls = {
    Ball(xyz.stem): Molecule.from_file(xyz) for xyz in BALLS_PATH.glob("*.xyz")
}
default_balls[Ball.custom] = None


class CustomBall:
    """ Define a custom ball to be used as projectile """

    def __init__(self, path: Union[str, Path] = None,
                 molecule: Molecule = None):
        """ Read the molecule structure from a file """
        if not molecule:
            try:
                self._molecule = Molecule.from_file(path)
            except FileNotFoundError:
                raise FileNotFoundError(f"File {path} not found.")

        elif isinstance(molecule, Molecule):
            self._molecule = molecule
        else:
            raise TypeError(
                "You must provide a path to a molecule file or a "
                "pymatgen Molecule object.\n"
                f"path: '{path}'\n molecule: '{molecule}'."
            )

    @property
    def molecule(self):
        return self._molecule
