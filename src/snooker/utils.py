# coding: utf-8

import numpy as np
import numpy.typing as npt
from scipy.linalg import null_space
from pymatgen.core import Molecule

BOLTZMANN = 1.380649e-23  # J.K-1


def gen_velocities(molecule: Molecule, temperature: float = 298.15,
                   seed: int = None) -> np.ndarray:
    """
    Initializes the velocities based on Maxwell-Boltzmann distribution.
    This function comes from pymatgen.io.vasp.inputs.Poscar class.
    https://pymatgen.org/pymatgen.io.vasp.inputs.html
    Removes linear, but not angular drift

    Scales the energies to the exact temperature (microcanonical ensemble)
    Velocities are given in A/fs => TODO check LAMMPS units

    Args:
        Molecule (Molecule): a pymatgen Molecule object
        temperature (float): Temperature in Kelvin.
        seed (int): the seed to initialize the random generator

    Returns:
        Array of velocities (Natom, 3) in A/fs (real Lammps units)
    """
    if seed:
        np.random.seed(seed)

    # mean 0 variance 1
    velocities = np.random.standard_normal((len(molecule), 3))

    # in AMU, (N,1) array
    atomic_masses = np.array(
        [site.specie.atomic_mass.to("kg") for site in molecule])
    dof = 3 * len(molecule) - 3  # -3 because remove linear drift

    # scale velocities due to atomic masses
    # mean 0 std proportional to sqrt(1/m)
    velocities /= atomic_masses[:, np.newaxis] ** (1 / 2)

    # remove linear drift (net momentum)
    velocities -= np.sum(atomic_masses[:, np.newaxis] * velocities, axis=0)
    velocities /= np.sum(atomic_masses)

    # scale velocities to get correct temperature
    energy = np.sum(1 / 2 * atomic_masses * np.sum(velocities**2, axis=1))
    scale = (temperature * dof * BOLTZMANN / (2 * energy)) ** (1 / 2)

    # style real : velocity = Angstroms/femtosecond
    # m.s-1 = (1e10 A) x (1e15 fs)-1
    velocities *= scale * 1e-5  # these are in A/fs

    return velocities   # in A/fs


def gen_com_velocities(molecule: Molecule, com_vel: npt.ArrayLike,
                       seed: int = None) -> np.ndarray:
    """ This function generates random velocities of the atoms of the given
    molecule, such as the velocity of the center of mass will be equal to
    the vector given as input.

    This function was written from the answer of Ben Grossmann:
    https://math.stackexchange.com/questions/4605931/

    Args:
        Molecule (Molecule): a pymatgen Molecule object
        com_vel (array): velocities of the center of mass
        temperature (float): Temperature in Kelvin.
        seed (int): the seed to initialize the random generator

    Returns:
        Random atomic velocities leading to the center of mass velocity
        given as input.
    """
    try:
        com_vel = np.array(com_vel, dtype=np.float64).reshape(3)
    except ValueError as e:
        print(e)
        raise ValueError(f"Center of mass velocity must be a vector of floats"
                         f" of length 3. com_vel is '{com_vel}'")

    natoms = len(molecule)
    # masses
    # in AMU, (N,1) array
    atomic_masses = np.array(
        [site.specie.atomic_mass.to("kg") for site in molecule],
        dtype=np.float64
    )
    # here are te weight used to compute the velocities
    w = atomic_masses / atomic_masses.sum()

    # guess matrix
    V0 = np.hstack([com_vel[:, None]] * natoms)
    B = null_space(w[None, :]).T
    A = np.random.standard_normal(size=(3, natoms - 1))

    velocities = V0 + np.dot(A, B)

    return velocities.transpose()


def xyz_file(species, coords):

    xyz = f""" {len(species)}\n xyz file of the molecule \n"""

    for iat, (specie, coord) in enumerate(zip(species, coords), 1):
        xyz += f" {specie}  "
        xyz += "".join([f"{x:12.6f}" for x in coord])
        xyz += "\n"

    return xyz
