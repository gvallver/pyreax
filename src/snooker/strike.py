# encoding: utf-8


from pathlib import Path

import numpy as np
from numpy.typing import ArrayLike
from pymatgen.core import Molecule, Element
from pymatgen.core.operations import SymmOp
import lammps

import pyreax
from snooker.utils import gen_velocities, gen_com_velocities
from snooker.controls import SnookerControls
from snooker.balls import Ball
from snooker import lammps_inputs

from scipy.constants import Boltzmann as k_B
from scipy.constants import Avogadro as N_A


class BaseStrike:
    """ This class is a base class for defining calculations in
    SnookRMol. It provides the initialization of the simulations from
    the controls parameters and general methods to set up the initial
    configuration on which the simulation will be performed. """

    def __init__(self, controls: SnookerControls = None, **kwargs):
        """ The ball (or projectile) and target molecules are defined
        as pymatgen.core.Molecule objects.

        Args:
            ball (Molecule): The molecule used as a ball (projectile)
            target (Molecule): The target molecule
        """
        # set default values
        if not controls:
            controls = SnookerControls()
        self.controls = controls

        # read and move the target molecule to the center of mass
        target = Molecule.from_file(self.controls.target)
        self.target = target.get_centered_molecule()

        # load the ball molecule and move it to the center of mass
        if self.controls.ball == Ball.custom:
            ball = Molecule.from_file(self.controls.ball_path)
        else:
            ball = self.controls.ball.molecule
        self.ball = ball.get_centered_molecule()

        # set up a list of elements strings
        self.elements = self.target.composition.elements
        self.elements += self.ball.composition.elements
        self.elements = list(set(self.elements))
        isort = 0
        if Element("C") in self.elements:
            i = self.elements.index(Element("C"))
            self.elements.insert(isort, self.elements.pop(i))
            isort += 1
        if Element("H") in self.elements:
            i = self.elements.index(Element("H"))
            self.elements.insert(isort, self.elements.pop(i))
            isort += 1
        self.elements = self.elements[:isort] + sorted(self.elements[isort:])
        # save it as a list of string
        self.elements = [el.symbol for el in self.elements]

        # set up the grid
        self.grid = self.controls.get_grid()

        # Result directory
        self.res_path = Path.cwd() / self.controls.results

    def move_ball_to_point(self, point: ArrayLike,
                           random_rotate: bool = True) -> Molecule:
        """ Apply a random rotation to the ball molecule and move it to
        to a given grid point. Then the ball is concatenated with the
        target and the whole system is returned.

        Args:
            point (array): R^3 point grid on which molecule will be translated
            random_rotate (bool): if True, the molecule is randomly rotate

        Returns:
            The compl

        """
        ball = self.ball.copy()

        # add a random rotation
        if random_rotate:
            op = SymmOp.from_origin_axis_angle(
                (0, 0, 0),
                axis=np.random.rand(3),
                angle=np.random.uniform(-180, 180),
            )
            ball.apply_operation(op)

        # move the center of mass of the ball to the grid point
        ball.translate_sites(vector=point)

        # concatenate ball and target molecules
        # fragment property is "target" or "ball" to keep the origin
        molecule = self.target.copy()
        molecule.add_site_property("fragment", len(molecule) * ["target"])
        for site in ball:
            molecule.append(
                site.species, site.coords,
                properties=dict(fragment="ball")
            )

        return molecule

    def gen_initial_velocities(self, ball_direction: ArrayLike,
                               ball_velocity: float = 1) -> ArrayLike:
        """ Generate the initial velocities for the atoms of the target
        and the ball molecules. The velocities of the atoms of the target
        molecule are sampled from the boltzmann distribution according to
        the target temperature defined in the controls parameters.

        The velocities of the ball's atoms, are computed such as the
        velocity of the center of mass of the ball was aligned along
        the ``ball_direction`` vecteur, in arguments and the norm of the
        veolocity of the center of mass, equal to the ``ball_velocity``
        in arguments. The ``ball_direction`` vector is not normalized
        internally, and can be used directly, using the default value of
        1 (A.fs-1) for the ``ball_velocity``.

        For consistency, all units are in A.fs-1. In consequence, the
        components of the ``ball_direction`` or the ``ball_velocity``
        **must be in A/fs**.

        Note that this method does not need to know the relative position
        of the ball and the target. The target and ball molecule are
        only used to consider atomic masses.

        Args:
            mol (Molecule): The complete system, target + ball
            ball_direction (array): The vector along which the ball will
                be directed. The velocity of the center of mass of the
                ball will be aligned in this direction.
            ball_velocity (float): Norm of the velocity of the center
                of mass of the ball, default to 1 (A.fs-1)

        Returns
            An array of shape N x 3 with velocities components of each atom
            in A.fs-1 (real lammps unit).
        """
        try:
            ball_direction = np.array(
                ball_direction, dtype=np.float64).reshape(3)
        except ValueError as error:
            print(error)
            raise ValueError(
                "The ball_direction must be an array of float of"
                f" length 3, ball_direction is: '{ball_direction}'")

        ball_velocities = gen_com_velocities(
            self.ball, ball_velocity * ball_direction
        )
        target_velocities = gen_velocities(
            self.target, temperature=self.controls.target_temperature,
        )

        return np.concatenate([target_velocities, ball_velocities], axis=0)

    def _gen_initial_velocities(self, mol: Molecule, point: ArrayLike,
                                T: float, ) -> ArrayLike:
        """ Generate the initial velocities for the target molecule and the
        ball molecule.

        Args:
            mol (Molecule): The complete system, target + ball
            point (array): The point to which the ball will be directed

        Returns
            An array of shape N x 3 with velocities components of each atom.
        """

        M_O = 15.999 * 1e-3  # kg.mol-1
        M_H = 1.008 * 1e-3  # kg.mol-1

        # Vector O

        v1O = np.sqrt(3 * k_B * T / (M_O / N_A))
        dec1 = str(v1O * 1e10 * 1e-15)
        # 1e10 x 1e-15 => conversion to A.fs-1

        # number of decimal for later velocity generation
        count = 0
        for c in dec1:
            if c == '0':
                count += 1
            if c == '.':
                continue
            else:
                pass

        cent = str(1)
        for x in range(count+1):
            cent += str(0)

        decO = 1/float(cent)

        # VECTOR H

        v2H = np.sqrt(3 * k_B * T / (M_H / N_A))
        dec2 = str(v2H * 1e10 * 1e-15)

        # number of decimal
        count = 0
        for c in dec2:
            if c == '0':
                count += 1
            if c == '.':
                continue
            else:
                pass

        cent = str(1)
        for x in range(count+1):
            cent += str(0)

        decH = 1/float(cent)

        # Updating the velocities adding the velocities of OH
        vel = gen_velocities(mol)  # Velocities molecules, Default 298.15K

        u = - point.copy()  # The opposite point on the spehre coordinate for the direction
        u /= np.linalg.norm(u)  # norm of the OH vector velocity
        vH = np.random.normal(v2H * 1e10 * 1e-15, decH) * u  # A.fs-1
        vO = np.random.normal(v1O * 1e10 * 1e-15, decO) * u  # A.fs-1

        vels = np.concatenate([vel, vO[np.newaxis], vH[np.newaxis]])
        # print(vels)

        return vels

    def export_grid(self, filename: str = "points.xyz", xyz: bool = False):
        """ Export the grid points in a xyz format """

        header = f"{len(self.grid)}\ngrid of initial points\n"
        if xyz:
            Molecule(len(self.grid) * ["H"], self.grid).to(
                fmt="xyz", filename=filename)
        else:
            np.savetxt(filename, self.grid, fmt="%12.6f", header=header)

    def run(self):
        """ This method is used in child classes to produce the input
        files and or run the simulations.
        In this base class, this method only produce the initial
        geometries and return the list of molecule object.
        """
        return [molecule for _, molecule in self]

    def output(self, fmt: str = "xyz"):
        """ Output the configuration with the target molecule at the
        center of mass and the ball molecule at each point of the grid
        to files with the given format in the given path.

        The format must be suported by the pymatgen output method of
        the Molecule class.

        If path does not exist, the method try to create it.

        Args:
            fmt (str): file format for the geometries. Default is xyz
            path (str): path to store the output file. Default is a
                geom folder in the working directory.
        """

        try:
            # make output dir if it does not exist
            self.res_path.mkdir(parents=True, exist_ok=False)
        except FileExistsError as error:
            print(error)
            raise FileExistsError(
                f"Folder '{self.res_path}' exists. Rename it or choose"
                " a different name."
            )

        for i, (_, molecule) in enumerate(self):
            molecule.to(fmt, filename=self.res_path / f"mol{i:03d}.{fmt}")

    def __iter__(self):
        for point in self.grid:
            # move the center of mass of the ball to the grid point add
            # a random rotation and concatenate with the target molecule
            molecule = self.move_ball_to_point(
                point,
                random_rotate=self.controls.rotate_ball
            )
            yield point, molecule

    def __getitem__(self, key: int):
        point = self.grid[key]
        molecule = self.move_ball_to_point(
            point, random_rotate=self.controls.rotate_ball
        )
        return point, molecule


class LammpsStrike(BaseStrike):
    """ This class will perform the calculations using LAMMPS """

    def point_setup(self, molecule: Molecule, ball_direction: ArrayLike,
                    ball_velocity: float = 1, data_file: str = "system.data"):
        """ Make input files for a lammps run.

        Args:
            data_file (str): name of the lammps data file

        Returns
            String representations of the lammps input file and lammps
            data file.
        """

        # compute initial velocities
        velocities = self.gen_initial_velocities(ball_direction, ball_velocity)

        # FABRICATION DU DATAFILES + L ORDRE DS SPECIES POUR LE INPUT FILE
        lmp_data, _ = lammps_inputs.data_file(
            molecule, velocities, box_size=self.controls.box_size,
            species_order=self.elements
        )

        # FABRICATION DE L INPUT FILE
        # Gives the force field location
        lmp_inp = lammps_inputs.input_file(
            self.controls.ffield,
            data_file=data_file,
            elements=self.elements,
            n_step=self.controls.n_step,
            # dump_trj=100
        )

        return lmp_inp, lmp_data

    def run(self):
        """ Run the reactive trajectories using LAMMPS according to the
        control parameters """

        n_reactions = 0

        for i, (point, molecule) in enumerate(self):

            print(f"run {i}")

            # compute velocity
            ball_direction = point
            ball_velocity = 1

            # set up lammps input file and write lammps data file
            data_file = "tmp.data"
            lmp_inp, lmp_data = self.point_setup(
                molecule, ball_direction, ball_velocity, data_file)
            with open(data_file, "w") as fdata:
                fdata.write(lmp_data)

            try:
                # run lammps calculations
                lmp = lammps.lammps(
                    cmdargs=["-log", "none", "-screen", "none",  "-nocite"])
                lmp.commands_string(lmp_inp)
                lmp.close()

                str_name = self.analyze_trajectory("bonds.dat", data_file)

                if str_name:
                    print('reaction' + str_name)
                    n_reactions += 1
                    with open("sucess/clbz"+str(n_reactions)+".data", "w") as fdata:
                        fdata.write(data_file)

                    f2 = open("sucess/reaSnipe.txt", "a")
                    f2.write(str_name+'\n')
                    f2.close()

                    f3 = open("sucess/reapoint.txt", "a")
                    f3.write(str(point)+'\n')
                    f3.close()
                    print('reaction')

            except:
                print("Unexpected error")

    def analyze_trajectory(self, bonds_file: str = "bonds.data",
                           data_file: str = "system.data") -> str:
        """ Perform an analysis of the trajectory based on the bond order.
        In this method, the number of bonds as computed by the REAX module
        of lammps is used to identify if a reaction occurred. The method
        needs the output file of the fix reaxff/bonds and the data
        file of the run.

        Args:
            bonds_file (str): path to the output file of the fix reaxff/bonds
            data_file (str): path to the lammps data file

        Returns
            A string representation of the reaction.

        TODO: Use only the number of bonds is dangerous. For example,
        in the case of a molecular rearrangement, the total number of
        bonds may be the same but the connectivity is different.
        """

        rbo = pyreax.ReaxBO(bonds_file, data_file, self.elements)

        # DATAFRAME of INITIAL bond order values
        df1 = rbo.atom_data[rbo.atom_data["step"] == 1].copy()
        df1 = df1.set_index('iat')

        # DATAFRAME of FINAL bond order values
        last_step = rbo.atom_data.step.max()
        df2 = rbo.atom_data[rbo.atom_data["step"] == last_step].copy()
        df2 = df2.set_index('iat')

        df1["connectivity_diff"] = df1["nb"] - df2["nb"]

        # LISTE OF CONNECTIVITY
        # connectivity_diff = df1['nb'] - df2['nb']

        # PARAMETERS INITIALISATION
        reaction = False
        iat_rea = list()
        kind_of_rea = list()

        # Check of connectivity change otherwise Reaction = False
        for iat, row in df1.iterrows():
            # for iat, line in enumerate(connectivity_diff):
            if row.connectivity_diff != 0:
                reaction = True
                iat_rea.append(iat)

                if row.connectivity_diff == 1:
                    # atom loose a bond
                    kind_of_rea.append('brk')

                elif row.connectivity_diff == -1:
                    # atom has a new bond
                    kind_of_rea.append('form')

                # TODO:
                # what about if connectivity diff is more than 1
                else:
                    kind_of_rea.append("other")

        # If a reaction occur creates the name of the reaction
        str_name = str()
        if reaction:
            for iat in iat_rea:
                species = df1.at[iat,  "element"]
                str_name += f"{species}-{iat_rea[iat]}-{kind_of_rea[iat]} "

            check_H_sharing = str_name.split()

            if df1.connectivity_diff.sum() == -2:
                if check_H_sharing[0][0] == 'H':
                    if check_H_sharing[1][0] == 'O':
                        print('H-sharing')
                        reaction = False

        return str_name


class XTBStrike(BaseStrike):
    """ This class will perform the calculations using XTB """
