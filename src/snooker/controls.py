# encoding: utf-8

""" This module defines the default control parameters in SnookRMol """

from pathlib import Path
from enum import Enum, unique

from pydantic import BaseModel, PositiveFloat, validator, PositiveInt

from pymatgen.core import Molecule
import spheric_grid
from snooker.balls import Ball


@unique
class ImplementedSoftware(Enum):
    lammps = "lammps"
    orca = "orca"
    xtb = "xtb"


class Software(BaseModel):
    """ This class contains the data that defined the software to be used """
    kind: str = ImplementedSoftware.lammps
    executable: str = "lmp_serial"
    args: str = ""


class GridControls(BaseModel):
    """ This class contains the parameters of the grid to be used """
    grid_shape: spheric_grid.GridShape = spheric_grid.GridShape.sphere
    grid_type: spheric_grid.GridType = spheric_grid.GridType.regular
    radius: PositiveFloat = 5.0
    n_points: PositiveInt = 20


class SnookerControls(BaseModel):
    target: str
    ball: Ball = Ball.hydroxyl
    ball_path: str = None
    rotate_ball: bool = True

    results: str = "results"

    seed: int = -1

    software: Software = Software()
    grid: GridControls = GridControls()

    box_size: PositiveFloat = 15.0
    temperature: PositiveFloat = 300.0
    target_temperature: PositiveFloat = None
    ball_temperature: PositiveFloat = None
    n_step: PositiveInt = 30000
    ffield: str = ""

    @validator("target_temperature", "ball_temperature", always=True)
    def temp_validation(cls, temp, values):
        """ Set target and ball temperature to the value of temperature
        parameter. """
        if temp is None:
            return values["temperature"]

    @validator("target")
    def read_target(cls, target_file):
        path = Path(target_file)
        if path.is_file():
            return path
        else:
            raise FileNotFoundError(f"File '{path}' does not exist.")

    @validator("ball_path")
    def read_custom_ball(cls, ball_path, values):
        if values["ball"] == Ball.custom:
            path = Path(ball_path)
            if path.is_file():
                return path
            else:
                raise FileNotFoundError(f"File '{path}' does not exist.")

    @classmethod
    def from_dict(cls, kwargs):
        """ Set up a SnookerControls object. If some informations are
        missing, the default values will be used. """

        try:
            target = kwargs["target"]
        except KeyError:
            raise KeyError(f"Target molecule is not defined {kwargs}.")

        try:
            ball = Ball(kwargs["ball"])
        except KeyError:
            ball = Ball.hydroxyl

        try:
            software = Software(**kwargs["software"])
        except KeyError:
            software = Software()

        try:
            grid = GridControls(**kwargs["grid"])
        except KeyError:
            grid = GridControls()

        return cls(target, ball, software, grid)

    def get_grid(self):
        """ Return the grid according to the parameters """
        if self.grid.grid_shape == spheric_grid.GridShape.sphere:
            grid = spheric_grid.SphericGrid(
                n_points=self.grid.n_points,
                radius=self.grid.radius,
                grid_type=self.grid.grid_type,
            )
        elif self.grid.grid_shape == spheric_grid.GridShape.fused_sphere:
            mol = Molecule.from_file(self.target)
            mol = mol.get_centered_molecule()
            grid = spheric_grid.FusedSpheresGrid(
                 centers=mol.cart_coords,
                 radius=self.grid.radius,
                 n_points=self.grid.n_points,
                 grid_type=self.grid.grid_type,
            )

        return grid.grid
