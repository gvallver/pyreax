#!/usr/bin/env python
# coding: utf-8

import numpy as np

from pymatgen.core import Molecule, Element


def data_file(molecule: Molecule, velocities: np.ndarray = None,
              species_order: list[str] = None,
              box_size: float = 40.0) -> str:
    """ produce the data file to be read with `read_data` in lammps

    Args:
        molecule (Molecule): molecule object
        velocities (ndarray): velocities for each atom in A.fs-1
        species_order (list): list of species name to define types
        box_size (float): size of the simulation box

    """

    # atom types order
    if not species_order:
        # atom_types = sorted(
        #   [el.symbol for el in molecule.composition.elements])
        atom_types = molecule.composition.elements
        # sort elements (alphabetic)
        atom_types = sorted(molecule.composition.elements,
                            key=lambda x: x.symbol)
    else:
        atom_types = [Element(el) for el in species_order]

    data = "LAMMPS data file with velocities\n\n"
    data += f"{len(molecule):8d} atoms\n"
    data += f"{len(atom_types):8d} atom types\n\n"
    data += f"{0:8.3f} {box_size:8.3f}  xlo  xhi\n"
    data += f"{0:8.3f} {box_size:8.3f}  ylo  yhi\n"
    data += f"{0:8.3f} {box_size:8.3f}  zlo  zhi\n\n"
    data += "Masses\n\n"

    for itype, element in enumerate(atom_types, 1):
        data += f"{itype:8d} {element.atomic_mass: 10.3f}\n"

    data += "\nAtoms\n\n"

    # move atoms at the center of the box
    coords = molecule.cart_coords.copy() - molecule.center_of_mass
    coords += np.array([box_size / 2] * 3)
    for iat, site in enumerate(molecule, 1):
        data += f"{iat:8d} {atom_types.index(site.specie) + 1:5d}   0.000"
        data += "".join([f"{x:12.6f}" for x in coords[iat - 1]])
        data += "\n"

    # if provided, write the velocities section
    if velocities is not None:
        data += "\nVelocities\n\n"

        for iat, v in enumerate(velocities, 1):
            data += f"{iat:8d}"
            data += "".join([f"{vi:12.6f}" for vi in v])
            data += "\n"

    return data, atom_types


DEFAULT_INPUT_FILE = {
    "units": "real",
    "atom_style": "charge",
    "boundary": "p p p",
    "pair_style": "reaxff NULL",
    "run_style": "verlet",
    "neighbor": "2.0 bin",
    "neigh_modify": "every 10 delay 0 check no",
    "timestep": "0.1",
}


def input_file(ffield: str, elements: list[str], n_step: int = 1000,
               temperature: float = 298.15, thermostat: bool = False,
               data_file: str = "mol.data", n_repeat_species: int = 4,
               dump_trj: int = None, seed: int = 12345, **kwargs) -> str:
    """ Produce the input files

    Args:
        ffield (str): path to the force field file, for pair_coeff command
        elements (list): list of elements (list of string)
        n_step (int): number of step of the simulation (by default dt = 0.1 fs)
        temperature (float): temperature of the dynamics, if thermostat is
            False this argument is not considered.
        thermostat (bool): if True, a fix NVT is implemented according to the
            given temperature. Default is False
        data_file (str): path to the data file, for read_data command
        n_repeat_species (int): values of the Nrepeat parameters of the
          fix reaxff/species, default 4
        dump_trj (int): snapshot of the trajectory are dumps every this
            timestep. Default is None and trajectory is not saved.
        seed (int): seed for random number generator in LAMMPS
        kwargs: a key value syntax for the parameters of the simulation. All
            element must be a valid LAMMPS command.

    Returns:
        The input file as a string
    """

    # manage default parameters
    params = DEFAULT_INPUT_FILE.copy()
    params.update(**kwargs)

    # check if elements are sorted consistently with other files (data ?)
    elements = " ".join(elements)

    inp_file = "# Lammps input file\n\n"

    inp_file += f"units         {params.pop('units')}\n"
    inp_file += f"atom_style    {params.pop('atom_style')}\n"
    inp_file += f"boundary      {params.pop('boundary')}\n"
    inp_file += f"read_data     {data_file}\n"
    inp_file += f"pair_style    {params.pop('pair_style')}\n"
    inp_file += f"pair_coeff    * * {ffield} {elements}\n"
    inp_file += "fix            1 all qeq/reax 1 0.0 10.0 1e-6 reax/c\n"
    inp_file += f"run_style     {params.pop('run_style')}\n"
    inp_file += f"neighbor      {params.pop('neighbor')}\n"
    inp_file += f"neigh_modify  {params.pop('neigh_modify')}\n"
    inp_file += f"timestep      {params.pop('timestep')}\n"

    # add other parameters
    for param, value in params.items():
        inp_file += f"{param}   {value}\n"

    inp_file += f"write_dump all xyz first.xyz modify element {elements}\n"

    inp_file += (f"fix spe all reaxff/species 1 1 "
                 f"1 species_init.out element {elements}\n")

    inp_file += "run 1\n"
    inp_file += "unfix spe\n"

    if thermostat:
        inp_file += (f"fix NVT all nvt temp {temperature:.2f} "
                     f"{temperature:.2f} $(100.0*dt)\n")
    else:
        inp_file += "fix NVE all nve\n"

    inp_file += f"variable nstep equal {n_step}\n"

    inp_file += f"fix bonds all reaxff/bonds {n_step} bonds.dat\n"
    inp_file += (f"fix species all reaxff/species 1 {n_repeat_species} "
                 f"{n_step} species.out element {elements}\n")

    if dump_trj:
        inp_file += (f"dump trj all custom {dump_trj} run.lammpstrj"
                     " id type element x y z\n")
        inp_file += f"dump_modify trj sort id element {elements}\n"

    inp_file += f"run {n_step}\n"
    inp_file += f"write_dump all xyz final.xyz modify element {elements}\n"
    return inp_file
