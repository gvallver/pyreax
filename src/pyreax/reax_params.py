#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains a class to read and store the ReaxFF parameters from 
ffield files.
"""

__all__ = ["ReaxParams"]


class ReaxParams:
    """ Store ReaxFF parameters """

    def __init__(self, filename):
        """ Read the parameters from the given file 
        
        Args:
            file (str or File): file path or File object
        """
        # blocks name
        self.blocks = ["general", "atoms", "bonds", "offdiag", "angles",
                       "torsions", "hbonds"]
        self.blocks_els = dict()
        self.params_keys = dict()

        # Store all parameters in a dict
        self._params = dict()

        # parse parameter file
        try:
            # try to open file
            with open(filename, "r", encoding="utf-8") as fparam:
                self._parse(fparam)
        except TypeError:
            # parse the file object
            self._parse(filename)


    def _parse(self, f):
        """ Read reaxFF parameters in the given file and fill in the 
        self._params dictionary
        
        Args:
            f (File): file oject
        """

        f.readline()  # title line

        # number of general parameters
        n_g_param = int(f.readline().split()[0])

        # read general parameters
        self._params["general"] = dict()
        gkeys = ["p_boc1", "p_boc2", "p_coa2", "p_trip4", "p_trip3", "k_c2",
                    "p_ovun6", "p_trip2", "p_ovun7",
                    "p_ovun8", "p_trip1", "swa", "swb", "x1", "p_val7", "p_lp1",
                    "p_val9", "p_val10", "x2",
                    "p_pen2", "p_pen3", "p_pen4", "x3", "p_tor2", "p_tor3",
                    "p_tor4", "x4", "p_cot2", "p_vdw1",
                    "cutoff", "p_coa4", "p_ovun4", "p_ovun3", "p_val8", "x5",
                    "x6", "x7", "x8", "p_coa3"]
        for i in range(n_g_param):
            val = float(f.readline().split()[0])
            if i < len(gkeys):
                self._params["general"][gkeys[i]] = val
            else:
                self._params["general"][f"g_par_{i}"] = val

        # number of atoms
        natoms = int(f.readline().split()[0])
        _ = [f.readline() for _ in range(3)]  # read 3 lines

        # read atoms parameters
        self._params["atoms"] = dict()
        akeys = ["ro_sigma", "Val", "mass", "Rvdw", "Dij", "gamma_EEM",
                    "ro_pi", "Val_e", "alfa", "gamma_vdW", "Val_angle",
                    "p_ovun5", "x1", "chiEEM", "etaEEM", "x2", "ro_pipi",
                    "p_lp2", "Heat_inc", "p_boc4", "p_boc3", "p_boc5", "x3",
                    "x4", "p_ovun2", "p_val3", "x5", "Val_boc", "p_val5", "x6",
                    "x7", "x8"]
        atom_list = list()
        for iat in range(natoms):
            line = f.readline()
            element = line.split()[0]
            atom_list.append(element)
            at_params = [float(val) for val in line.split()[1:]]
            for _ in range(3):  # 3 lines
                at_params += [float(val) for val in f.readline().split()]
            self._params["atoms"][element] = {
                k: v for k, v in zip(akeys, at_params)}
            # WARNING: This key is not in the `akeys` list. In consequence
            # it hasn't got a keys id and cannot be return from a parameter
            # id
            # number assigned to atom
            self._params["atoms"][element]["itype"] = iat + 1

        # number of bonds
        nbonds = int(f.readline().split()[0])
        _ = f.readline()

        # read bonds parameters
        self._params["bonds"] = dict()
        bkeys = ["De_sigma", "De_pi", "De_pipi", "p_be1", "p_bo5", "13corr",
                    "p_bo6", "p_ovun1", "p_be2", "p_bo3", "p_bo4", "x1", "p_bo1",
                    "p_bo2", "over", "x2"]
        bond_list = list()
        for _ in range(nbonds):
            line = f.readline().split()
            i, j = int(line[0]), int(line[1])
            bond_list.append((i, j))
            bond_params = [float(val) for val in line[2:]]
            bond_params += [float(val) for val in f.readline().split()]
            self._params["bonds"][(i, j)] = {
                k: v for k, v in zip(bkeys, bond_params)}

        # number of off-diagonals
        noffdiag = int(f.readline().split()[0])

        # read off-diagonal terms
        self._params["offdiag"] = dict()
        okeys = ["D_ij", "Rvdw", "alfa", "ro_sigma", "ro_pi", "ro_pipi"]
        offdiag_list = list()
        for _ in range(noffdiag):
            line = f.readline().split()
            i, j = int(line[0]), int(line[1])
            offdiag_params = [float(val) for val in line[2:]]
            offdiag_list.append((i, j))
            self._params["offdiag"][(i, j)] = {
                k: v for k, v in zip(okeys, offdiag_params)}

        # number of angles
        nangles = int(f.readline().split()[0])

        # read angle terms
        self._params["angles"] = dict()
        angkeys = ["theta_0", "p_val1", "p_val2",
                    "p_coa1", "p_val7", "p_pen1", "p_val4"]
        ang_list = list()
        for _ in range(nangles):
            line = f.readline().split()
            i, j, k = int(line[0]), int(line[1]), int(line[2])
            ang_list.append((i, j, k))
            angles_params = [float(val) for val in line[3:]]
            self._params["angles"][(i, j, k)] = {
                l: v for l, v in zip(angkeys, angles_params)}

        # number of torsions
        ntorsions = int(f.readline().split()[0])

        # read torsion terms
        self._params["torsions"] = dict()
        tkeys = ["V1", "V2", "V3", "p_tor1", "p_cot1", "x1", "x2"]
        tors_list = list()
        for _ in range(ntorsions):
            line = f.readline().split()
            i, j, k, l = int(line[0]), int(
                line[1]), int(line[2]), int(line[3])
            tors_list.append((i, j, k, l))
            torsion_params = [float(val) for val in line[4:]]
            self._params["torsions"][(i, j, k, l)] = {
                m: v for m, v in zip(tkeys, torsion_params)}

        # number of hydrogen bonds
        nhb = int(f.readline().split()[0])

        # read angle terms
        self._params["hbonds"] = dict()
        hbkeys = ["r_hb", "p_hb1", "p_hb2", "p_hb3"]
        hbonds_list = list()
        for _ in range(nhb):
            line = f.readline().split()
            i, j, k = int(line[0]), int(line[1]), int(line[2])
            hb_params = [float(val) for val in line[3:]]
            hbonds_list.append((i, j, k))
            self._params["hbonds"][(i, j, k)] = {
                m: v for m, v in zip(hbkeys, hb_params)}

        self.blocks_els = {
            "general": None, "atoms": atom_list, "bonds": bond_list,
            "offdiag": offdiag_list, "angles": ang_list, "torsions": tors_list,
            "hbonds": hbonds_list
        }

        self.params_keys = {
            "general": gkeys, "atoms": akeys, "bonds": bkeys,
            "offdiag": okeys, "angles": angkeys, "torsions": tkeys,
            "hbonds": hbkeys
        }

    def get_param_ids(self, *args):
        """ Return the id of a paramters from the name of its block its coord
        and its parameter name """
        if len(args) == 2:
            if args[0] != "general":
                raise ValueError("get_params_ids(): block is not 'general', "
                                 "missing 1 argument.")
            block = 1
            try:
                param = self.params_keys["general"].index(args[1])
                param += 1
            except ValueError as error:
                print("%s is not a general parameter name." % args[1])
                raise error

            return block, None, param

        elif len(args) == 3:
            bname, cname, pname = args

            # block id
            try:
                block = self.blocks.index(bname)
                block += 1
            except ValueError as error:
                print("%s is not a block of parameter" % bname)
                raise error

            # coord id
            try:
                coord = self.blocks_els[bname].index(cname)
                coord += 1
            except ValueError as error:
                print("%s is not a coord name of block %s" % (cname, bname))
                raise error

            # coord id
            try:
                param = self.params_keys[bname].index(pname)
                param += 1
            except ValueError as error:
                print("%s is not a parameter name of block %s" %
                      (pname, bname))
                raise error

            return block, coord, param

    def get_param_keys(self, *args):
        """ Return the keys corresponding to the parameter with number `param` 
        in coordinate or element with number `coord` and in block with
        number `block`.

        Args:
            block (int): number of the block
            coord (int): number of the coord
            param (int): number of the parameters

        Returns:
            The keys that define a parameter: block name, coord name and parameter
            name.
        """
        if len(args) == 3:
            block, coord, param = args
            if block == 1:
                print("args: ", args)
                raise ValueError("get_params_keys(): depth of block general is"
                                 " 2 not 3. Too much arguments.")
        elif len(args) == 2:
            block, foo = args
            if block == 1:
                coord = None
                param = foo
            else:
                coord = foo
                param = None
        elif len(args) == 1:
            block = args[0]
            coord = None
            param = None
        else:
            print("args: ", args)
            if len(args) == 0:
                raise TypeError("get_param_keys() missing 1 required positional"
                                " argument: 'block'")
            else:
                raise TypeError("get_param_keys() too much arguments.")

        # get block name
        try:
            block_name = self.blocks[block - 1]
        except IndexError as error:
            print(block, self.blocks)
            raise error

        # get coordinate name
        if block == 1:
            coord_name = None
        else:
            coord_list = self.blocks_els[block_name]
            try:
                coord_name = coord_list[coord - 1]
            except IndexError as error:
                print(coord, coord_list)
                raise error

        if param:
            params_list = self.params_keys[block_name]
            try:
                param_name = params_list[param - 1]
            except IndexError as error:
                print(param, params_list)
                raise error
        else:
            param_name = None

        return block_name, coord_name, param_name

    def get_params(self, *args):
        """ Return the parameter `param` in coordinate or element `coord` and
        in block `block`.

        Args:
            block (int): number of the block
            coord (int): number of the coord
            param (int): number of the parameters

        Returns:
            The parameter
        """
        b_name, c_name, p_name = self.get_param_keys(*args)
        if c_name is None and p_name is None:
            return self._params[b_name]
        elif p_name is None:
            return self._params[b_name][c_name]
        elif c_name is None:
            return self._params[b_name][p_name]
        else:
            return self._params[b_name][c_name][p_name]

    def __getattr__(self, attr):
        """ Return the given block """
        if attr in self._params:
            return self._params[attr]

    def __getitem__(self, attr):
        """ Return a block or a parameters """
        if isinstance(attr, str):
            # return the block  of name attr
            if attr in self._params:
                return self._params[attr]

        elif isinstance(attr, int):
            try:
                block_name = self.blocks[attr]
                return self._params[block_name]
            except IndexError as error:
                print(attr, self.blocks)
                raise error

        elif isinstance(attr, (list, tuple)):
            if all([isinstance(el, int) for el in attr]):
                return self.get_params(*attr)
            else:
                print(attr)
                raise ValueError("wrong integers for parameters definition.")
        else:
            print(attr)
            raise ValueError("Wrong parameters definition.")
