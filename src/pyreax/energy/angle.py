#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute valence angle terms contribution 
to the energy of the system in ReaxFF.

Equation numbers refer to the following reference: 
Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. 
The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp, product


def E_val(p, i, j, k, rij):
    """
    Valence Angle energy
    Equation 13a
    """
    D_j = None  # !
    theta0_BO = None  # !
    theta_ijk = None  # !
    return f7(p, i, j)*f7(p, j, k)*f8(D_j) * \
        (p.angle[(i, j, k)]["pval1"]-p.angle[(i, j, k)]["pval1"] *
         exp(-p.angle[(i, j, k)]["pval2"](theta0_BO-theta_ijk)**2))


def f7(p, i, j, rij):
    """

    Equation 13b
    """
    BOij = None  # !
    return 1 - exp(-p.atoms[i]["p_val3"]*(BOij**p.angles[(i, j, k)]["p_val4"]))


def f8(p, i, j, k, rij):
    """

    Equation 13c
    """
    D_j_angle = None  # !
    num = 2 + exp(p.angles[(i, j, k)]["p_val6"] * D_j_angle)
    denom = (1 + exp(p.angles[(i, j, k)]["p_val6"] * D_j_angle)
             + exp(-p.general["p_val7"] * D_j_angle))
    frac = num / denom
    return p.atoms[i]["p_val5"] - (p.atoms[i]["p_val5"] - 1) * frac


def SBO(p, i, j, rij):
    """
    Sum of pi-bond orders
    Example: C-C (SBO=0 sp3 hybridisation, SBO=1 sp2, SBO=2 sp)
    Equation 13d
    """
    #! notion of BO neighbour list
    BOpi = None  # !
    BOpipi = None  # !
    BO = None  # !
    D_j_angle = delta_j_angle(p, i, j, rij)
    return sum(BOpi+BOpipi)+(1-product(exp(-BO**8)))*(-D_j_angle-p.general["p_val8"]*n_lp_j)


def delta_j_i(p, i, j, rij):
    """

    Equation 13e
    """
    BOjn = None  # !
    return p.atoms[i]["Val_j_angle"] + sum(BOjn)


def SBO2(SBO):
    """
    Corrected SBO to avoid singularities
    Equations 13f
    """
    if SBO <= 0:
        SBO2 = 0
    elif 0 < SBO < 1:
        SBO2 = SBO**p.general["p_val9"]
    elif 1 < SBO < 2:
        SBO2 = 2 - (2-SBO)**p.general["p_val9"]
    elif SBO > 2:
        SBO2 = 2
    return SBO2


def theta_0_BO(p, i, j, rij):
    """
    Calculated equilibrium angle depending on the number of pi bonds present
    Equation 13g
    """
    #! should take in BO (not just this equation)
    return np.pi - p.angles[(i, j, k)]["theta_0_0"]*(1-exp(-p.general["p_val10"]*(2-SBO2(SBO(p, i, j, rij)))))

# Penalty Energy


def E_pen(p, i, j, rij):
    """
    Penalty Energy to reproduce the stability of systems with double bonds sharing an atom in valency angle
    Equation 14a
    """
    BOij = bo(p, i, j, rij)
    BOjk = bo(p, j, k, rij)
    return p.angles[(i, j, k)]["p_pen1"]*f9(p, i, j, rij)*exp(-p.general["p_pen2"]*(BOij-2)**2)*exp(-p.general["p_pen2"]*(BOjk-2)**2)


def f9(p, i, j, rij):
    """

    Equation 14b
    """
    delta_j = None  # !
    return (2+exp(-p.general["p_pen3"]*delta_j)) / (1 + exp(-p.general["p_pen3"]*delta_j) + exp(p.general["p_pen4"]*delta_j))

# Three-body conjugation term


def E_coa():
    """
    Energy penalty for -NO2 conjugated systems
    Equation 15
    """
    D_j_val = delta_j_val(p, i, j, rij)
    frac1 = 1 / (1 + exp(p.general["p_coa2"]*D_j_val))
    BOij = bo(p, i, j, rij)
    BOin = bo(p, i, n, rij)
    BOjk = bo(p, j, k, rij)
    BOkn = bo(p, k, n, rij)
    ex1 = exp(-p.general["p_coa3"]*(-BOij + sum(BOin))**2)
    ex2 = exp(-p.general["p_coa3"]*(-BOjk + sum(BOkn))**2)
    ex3 = exp(-p.general["p_coa4"]*(BOij-1.5)**2)
    ex4 = exp(-p.general["p_coa4"]*(BOjk-1.5)**2)
    #! high memory usage
    return p.angles[(i, j, k)]["p_coa1"]*frac1*ex1*ex2*ex3*ex4
