#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute CO triple bond stabilisation energy needed to make CO stable and inert to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def E_trip(p,i,j,rij):
    """
    
    Equation 20
    Ref:
    """
    BOij = None #!
    D_i = None #!
    D_j = None #!
    ex1 = exp(-p.general["p_trip4"]*(sum(BOik) - BOij))
    ex2 = exp(-p.general["p_trip4"]*(sum(BOjk) - BOij))
    frac1 = (ex1+ex2)/(1+(25*exp(p.general["p_trip3"]*(D_i+D_j))))
    return p.general["p_trip1"]*exp(-p.general["p_trip2"]*(BOij-2.5)**2)*frac1
