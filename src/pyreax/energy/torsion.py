#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute torsion contribution to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def E_tors():
    """
    
    Equation 16a
    """
    BOij = bo(p,i,j,rij)
    BOjk = bo(p,j,k,rjk)
    BOkl = bo(p,k,l,rkl)
    BOjk_pi = bo_pi(p,j,k,rjk)
    Dj = None #!
    Dk = None #!
    return f10(BOij,BOjk,BOkl)*sin(theta_ijk)*sin(theta_jkl)* \
            0.5*p.torsions[(i,j,k,l)]["V1"]*(1 + cos(w_ijkl))+ \
            0.5*p.torsions[(i,j,k,l)]["V2"]*exp(p.torsions[(i,j,k,l)]["p_tor1"]*(BOjk_pi-1+f11(Dj,Dk))**2)*(1-cos(2*w_ijkl))+ \
            0.5*p.torsions[(i,j,k,l)]["V3"]*(1+cos(3*w_ijkl))

def f10(BOij,BOjk,BOkl):
    """
    
    Equation 16b
    """
    return f10b(BOij)*f10b(BOjk)*f10b(BOkl)

def f10b(BOij):
    return 1-exp(-p.general["p_tor2"]*BOij)

def f11():
    """
    
    Equation 16c
    """
    return (2+f11b(p.general["p_tor3"])) / (1+f11b(p.general["p_tor3"])+f11b(-p.general["p_tor4"]))

def f11b(p_tori):
    D_j_angle = None #!
    D_k_angle = None #!
    return exp(-p_tori*(D_j_angle,D_k_angle))

# Four body conjugation term
def E_conj():
    """
    
    Equation 17a
    """
    BOij = bo(p,i,j,rij)
    BOjk = bo(p,j,k,rjk)
    BOkl = bo(p,k,l,rkl)
    return f12(BOij,BOjk,BOkl)*p.torsions[(i,j,k,l)]["p_cot1"]*(1+(cos(w_ijkl)**2-1)*sin(theta_ijk)*sin(theta_jkl)

def f12():
    """
    
    Equation 17b
    """
    BOij = bo(p,i,j,rij)
    BOjk = bo(p,j,k,rjk)
    BOkl = bo(p,k,l,rkl)
    return f12b(BOij)*f12b(BOjk)*f12b(BOkl)
                     
def f12b(BO):
    return exp(-p.general["p_cot2"]*((BO-1.5)**2))
