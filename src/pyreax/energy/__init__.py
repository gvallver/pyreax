# coding: utf-8

""" This module implement functions to compute several energy terms
of the ReaxFF force field. 

Equation numbers refer to the following reference: 
Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. 
The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""