#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute non-bonded contribution to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def Tap(R_cut,rij):
    """
    Taper correction for non-bonded interactions
    Equation 21 and 22
    Ref:
    """
    T7 = (r_ij**7)*(20/(R_cut**7))
    T6 = (r_ij**6)*(-70/(R_cut**6))
    T5 = (r_ij**5)*(84/(R_cut**5))
    T4 = (r_ij**4)*(-35/(R_cut**4))
    #T3 = (r_ij**3)*0
    #T2 = (r_ij**2)*0
    #T1 = (r_ij)*0
    #T0 = 1
    #return T7+T6+T5+T4+T3+T2+T1+T0
    return T7+T6+T5+T4+1

def E_VdW(p,i,j,rij):
    """
    
    Equation 23a
    Ref: 
    """
    return Tap(p.general["R_cut"],rij)*p.atoms[i]["D_ij"]*(exp(p.aij*(1-(f13(p,i,j,rij)/r_vdw))) - 2*exp(0.5*aij*(1-(f13(p,i,j,rij)/r_vdw))))

def f13(p,i,j,rij):
    """
    Shielding term to avoid excessively high repulsions between bonded atoms and atoms shring a valence angle
    Equation 23b
    Ref: 
    """
    return ((rij**p.general["p_vdw1"])+((1*p.atoms["gamma_w"])**p.general["p_vdw1"]))**(1/p.general["p_vdw1"])

def E_coul(p,i,j,rij):
    """
    
    Equation 24
    Ref: 
    """
    C = 1/(4*np.pi*np.epsilon)
    return Tap(p.general["R_cut"],rij)*C*((q_i*q_j)/(rij**3+(1/(p.atoms["gamma_ij"])**3))**(1/3))
