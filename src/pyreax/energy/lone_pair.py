#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute the energy penalty due to the deviation of calculated number of lone pairs from optimal number of lone pairs to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def delta_i_e(p,i,j,rij):
    """
    This function computes the differences between the total number of electrons in the outer shell
    and the sum of bond orders around the atomic center
    Equation 7
    """
    BOij = bo(p, i, j, rij)
    return -p.atoms[i]["Val_e"] + sum(BOij)

def n_lp_i(p,i,j,rij):
    """
    This function computes the number of lone pairs around an atom
    Equation 8
    """
    D_i_e = delta_i_e(p,i,j,rij)
    return int(D_i_e / 2) + exp(-p.general["p_lp_1"]*(2+D_i_e-(2*int(D_i_e/2)))**2)

def delta_lp_i(p,i,j,rij):
    """
    Deviation of calculated number of lone pairs from optimal number of lone pairs
    Equation 9
    """
    n_lp_opt = None #! pymatgen? ou ffield?
    n_lp = n_lp_i(p,i,j,rij)
    return n_lp_opt - n_lp

def E_lp(p,i,j,rij):
    """
    Computation of the energy penalty due to the deviation of calculated number of lone pairs
    from the optimal number of lone pairs
    Equation 10
    """
    D_lp_i = delta_lp_i(p,i,j,rij)
    return (p.atoms[i]["p_lp_2"]*D_lp_i)/(1+ exp(-75*D_lp_i))
