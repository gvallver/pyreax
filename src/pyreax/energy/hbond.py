#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute bond order dependent hydrogen bond energy contribution for a X-H--Z system to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp, sin
import numpy as np
def E_Hbond(p,i,j,rij):
    """
    Hydrogen bond energy contribution depending on the bond order of XH and HZ distance and XHZ angle
    Equation 18
    Ref: 
    """
    BOxh = 1 #None #!
    theta_xhz = 3.14 #! np.pi
    return p.hbonds[(i,j,i)]["p_hb1"]*(1-exp(p.hbonds[(i,j,i)]["p_hb2"]*BOxh))*exp(p.hbonds[(i,j,i)]["p_hb3"]*((p.hbonds[(i,j,i)]["r_hb"]/rij) \
                                                + (rij/p.hbonds[(i,j,i)]["r_hb"]) -2))*((np.sin(theta_xhz/2))**8)

#! [(3,2,3)]
