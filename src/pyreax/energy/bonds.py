#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute bond contribution to the energy.

Equation numbers are from : van Duin et al. J. Phys. Chem. A 2001

"""

from numpy import exp

def bond_energy(boij, p, i ,j): #bond order between i and j, p parameter set, element i, element j
    """ Compute the uncorrected bond energy, equation 5 """
    ##! add which set of parameters to read depending on i and j
    ##! desigma si same pair, D_ij sinon??
    ##! pbe1 pbe2 depends on bonds type
    return - p.De_sigma * boij_sigma * exp(p.p_be1 * (1 - boij_sigma ** p.p_be2)) - p.De_pi * boij_pi - p.De_pipi * boij_pipi
