#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute the energy penalty and contribution due to the respective decrease of overcoordination in atoms containing broken-up lone electron pair and resonance of the pi-electron between attached undercoordinated atomic centers to the energy of the system in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def delta_i_lpcorr(p,i,j,rij):
    """
    Corrected overcoordination due to the presence of lone pair
    Equation 11b
    """
    D_i = None #!
    D_lp_i = delta_lp_i(p,i,j,rij)
    BO_pi = bo_pi(p,i,j,rij)
    BO_pipi = bo_pipi(p,i,j,rij)
    numer = 1 + p.general["p_ovun3"]*exp(p.general["p_ovun_4"]*(sum((D_i-D_lp_i)*(BO_pi+BO_pipi))))
    return D_i - (D_i_lp / numer)

def E_over(p,i,j,rij):
    """
    Energy penalty to take into account overcoordination not caused by the presence of
    broken-up lone pair electron
    Equation 11a
    """
    BOij = bo(p,i,j,rij)
    D_i_lpcorr = delta_i_lpcorr(p,i,j,rij)
    frac1 = sum(p.bonds[i]["p_ovun_1"]*p.bonds[i]["D_e_sigma"]*BOij)/(D_i_lpcorr+p.atoms[i]["Val_i"])
    frac2 = 1 / (1+exp(p.atoms[i]["p_ovun2"]*D_i_lpcorr))
    return frac1*D_i_lpcorr*frac2

def E_under(p,i,j,rij):
    """
    
    Equation 12
    """
    D_i_lpcorr = delta_i_lpcorr(p,i,j,rij)
    D_i = None #!
    BOpi = bo_pi(p,i,j,rij)
    BOpipi = bo_pipi(p,i,j,rij)
    frac1 = (1 - exp(p.general["p_ovun6"]*D_i_lpcorr)) / (1+exp(-p.atoms[i]["p_ovun2"]*D_i_lpcorr))
    frac2 = 1/(1+p.general["p_ovun_7"]*exp(p.general["p_ovun8"]*(sum(D_j - D_j_lp)(BOpi+BOpipi))))
    return -p.atoms[i]["p_ovun5"] * frac1 * frac2
