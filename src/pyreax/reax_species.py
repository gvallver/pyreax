#!/usr/bin/env python
# coding: utf-8

""" This module implements a class in order to read the output files
produced by the ``reaxff/species`` fix of LAMMPS.

https://docs.lammps.org/fix_reaxff_species.html
"""

import re
import pandas as pd
import numpy as np
from pymatgen.core.composition import Composition

__all__ = ["ReaxSpecies"]


class ReaxSpecies:
    """ This class provides methods to read chemical species listed in the
    output file produced by the ``reaxff/species`` fix of LAMMPS. """

    def __init__(self, filename="sepcies.out"):
        """ The class reads the output file of the reax/c/species fix
        of LAMMPS Reax/c USER-MODULE.

        Args:
            filename: Filename or file object of a Orca output file.
        """

        self.data_mol = []
        self.data = []
        self.molecules = []

        # parse file
        try:
            # try to open file
            with open(filename, "r", encoding="utf-8") as fout:
                self._parse(fout)
        except TypeError:
            # parse the file object
            self._parse(filename)

    def _parse(self, fout):
        """ Parse the file and fill in object and attributes """

        self.molecules = set()
        self.data = list()
        for line in fout:
            molecules = [Composition(mol).alphabetical_formula
                         for mol in line.split()[4:]]

            self.molecules.update(set(molecules))

            line = fout.readline()
            vals = line.split()

            step = {
                "TimeStep": int(vals[0]),
                "No_moles": int(vals[1]),
                "No_Specs": int(vals[2]),
            }
            if len(molecules) != len(vals[3:]):
                raise ValueError("Number and names of molecules error")

#            molecules = {mol: int(n) for mol, n in zip(molecules, vals[3:])}
            temp_df = pd.DataFrame({"mol": molecules, "n": vals[3:]})
            temp_df = temp_df.astype({"n": "int64"})
            temp_df = temp_df.groupby("mol").agg("sum")
            step.update(temp_df.to_dict()["n"])

            self.data.append(step)

        self.data = pd.DataFrame(self.data)
        self.data.set_index("TimeStep", inplace=True)

        # molecule table
        lines = list()
        for mol in self.molecules:
            cmol = Composition(mol)
            compo = cmol.get_el_amt_dict()
            line = {"molecule": mol}
            line.update(compo)

            lines.append(line)

        dfmol = pd.DataFrame(lines)
        dfmol.fillna(0., inplace=True)
        if "H" in dfmol.columns:
            dfmol["H/C"] = dfmol["H"] / dfmol["C"]
            dfmol["DBE"] = dfmol["C"] - dfmol["H"] / 2 + 1

        self.data_mol = dfmol

    def get_proportions(self, last=False, minval=0.):
        """ Get the proportion of each species along the simulations

        Args:
            last (bool): if True, only the proportion at the last step is
                returned
            minval (float): filter the table and return only species for
                which at one given time the proportion is greater than
                min val. Default 0.
        """

        # make a copy of data
        df = self.data.copy()

        # compute the proportion in percent of each species
        df.iloc[:, 2:] = df.iloc[:, 2:].divide(df.No_moles, axis="index") * 100

        # filter according to the maximum proportion at a given timestep
        if minval > 0:
            selected_columns = list(df.columns[:2])
            for col, values in df.iloc[:, 2:].iteritems():
                if values.max() > minval:
                    selected_columns.append(col)

            df = df[selected_columns]

        # if last is true, return only the proportion at the end
        if last:
            last = df.iloc[[-1], 2:]
            last = last.dropna(axis="columns").transpose()
            last.index.name = "Species"
            last.columns = ["Proportion"]
            df = last.sort_values("Proportion", ascending=False)

        return df
