#!/usr/bin/env python
# coding: utf-8

""" This module implements a class in order to read the output files
produced by the ``reaxff/bonds`` fix of LAMMPS.

https://docs.lammps.org/fix_reaxff_bonds.html
"""

import re
import pandas as pd
import numpy as np

__all__ = ["ReaxBO"]


class ReaxBO:
    """ This class provides methods to read bonds and bond order results
    computed by the ``reaxff/bonds`` fix of LAMMPS. """

    def __init__(self, filename, lmp_data, elements=None):
        """ The class reads the output file that contains bond and
        bond order information

        Args:
            filename: File path or file object
            lmp_data: a lammps data file (just to read atom types)
            elements: elements' name in the same order as type
        """
        if not elements:
            self.elements = lambda i: f"X{i}"
            self._elements = None
        else:
            self._elements = elements
            self.elements = lambda i: self._elements[i - 1]

        # read atom types
        self.atom_types = self._read_atom_types(lmp_data)
        self.n_atoms = len(self.atom_types)

        # parse file
        try:
            # try to open file
            with open(filename, "r", encoding="utf-8") as fout:
                self.atom_data, self.bond_data, self.connectivity = \
                    self._parse(fout)
        except TypeError:
            # parse the file object
            self.atom_data, self.bond_data, self.connectivity = \
                self._parse(filename)

    @staticmethod
    def _read_atom_types(filename):
        """ Read atoms' type in a lammps data file """

        with open(filename, "r") as f:
            for line in f:
                if m := re.match(r"^\s*(\d+)\s+atoms", line):
                    n_atoms = int(m.group(1))
                elif "Atoms" in line:
                    break

            atom_types = list()
            f.readline()
            for iat in range(n_atoms):
                attype = int(f.readline().split()[1])
                atom_types.append(attype)

        return atom_types

    def _parse(self, fout):
        """ Parse the file and fill in object and attributes """

        data = list()
        bond_data = list()
        connectivity = list()

        for line in fout:
            if line[:11] == "# Timestep ":
                # start reading new time step
                timestep = int(line.split()[-1])

                [fout.readline() for _ in range(6)]

                step_connectivity = set()

                line = fout.readline()
                while "#" not in line:
                    vals = line.split()

                    # extract data
                    iat = int(vals[0])
                    itype = int(vals[1])
                    nb = int(vals[2])
                    jats = [int(val) for val in vals[3: 3 + nb]]
                    bonds = [tuple(sorted([iat, jat])) for jat in jats]
                    bos = [float(val)
                           for val in vals[3 + nb + 1: 3 + nb + 1 + nb]]

                    for jat, bo in zip(jats, bos):
                        if jat < iat:
                            # avoid duplicated bond
                            continue

                        # set bond data
                        jtype = self.atom_types[jat - 1]
                        bond_el = f"{self.elements(itype)}-{self.elements(jtype)}"
                        bond_data.append(dict(
                            step=timestep,
                            iat=iat,
                            itype=itype,
                            ielement=self.elements(itype),
                            nb=nb,
                            jat=jat,
                            jtype=jtype,
                            jelement=self.elements(jtype),
                            bond=bond_el,
                            bond_order=bo,
                        ))

                    # set atomic data
                    data.append(dict(
                        step=timestep,
                        iat=iat,
                        itype=itype,
                        element=self.elements(itype),
                        nb=nb,
                        abo=float(vals[-3]),
                        q=float(vals[-1]),
                        nlp=float(vals[-2]),
                    ))

                    step_connectivity.update(bonds)
                    line = fout.readline()

                connectivity.append(step_connectivity)

        # data frame with atom data
        atom_data = pd.DataFrame(data)
        atom_data.sort_values(by=["step", "iat"], inplace=True)
        atom_data.reset_index(inplace=True, drop=True)

        # data frame with bond data
        bond_data = pd.DataFrame(bond_data)
        bond_data.sort_values(by=["step", "iat", "jat"], inplace=True)
        bond_data.reset_index(inplace=True, drop=True)
        
        return atom_data, bond_data, connectivity
