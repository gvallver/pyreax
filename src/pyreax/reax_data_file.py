# coding: utf-8

""" This module implement a class in order to read a LAMMPS data file
in the special context of ReaxFF simulations. In particular it means
that only the charge atom type is supported and only the box dimensions
atom types and the Atoms sections are read.
"""

import re
import numpy as np
from typing import NamedTuple, TextIO, Union
from pymatgen.core import Structure, Lattice

__all__ = ["ReaxDataFile"]


class Atom(NamedTuple):
    """ A namedtuple to manage atom types, mass, elements and charge """
    itype: int = 0
    mass: float = 0.
    element: str = "X"
    charge: float = 0.


class ReaxDataFile:
    """ This class represents a Lammps Data file for use with ReaxFF. It
    means that it contains only the list of atoms in a charge atom type of
    Lammps. """

    def __init__(self, structure: Structure, atoms: list[Atom],
                 atom_types: dict, title: str = ""):
        """ Set up the data to write the file:

        Args:
            structure (Structure): pymatgen structure
            atoms: list of Atom
            atom_types: list of atom_types
            title: a title

        """
        self.structure = structure
        self.atoms = atoms
        self.atom_types = atom_types
        self.title = title

    def export_lammps_data(self, filename: str = "", box_shift: float = 0.0):
        """ Export the force field for LAMMPS in data format

        Args:
            filename (str): filename in which data are exported
            box_shift (float): empty space in angstrom added on each side
              of the box.
        """

        # header
        lines = f'LAMMPS data file {self.title}\n\n'

        lines += f'{len(self.structure):8d} atoms\n'
        lines += f'{len(self.atom_types):8d} atom types\n'
        lines += "\n"

        # bounding box values
        xmin, ymin, zmin = self.structure.cart_coords.min(axis=0)
        xlength, ylength, zlength = self.structure.lattice.abc
        lines += f"{xmin - box_shift:6.2f} {xmin + xlength + box_shift:6.2f}  xlo  xhi\n"
        lines += f"{ymin - box_shift:6.2f} {ymin + ylength + box_shift:6.2f}  ylo  yhi\n"
        lines += f"{zmin - box_shift:6.2f} {zmin + zlength + box_shift:6.2f}  zlo  zhi\n"

        # masses
        lines += '\n Masses\n\n'
        for itype, atom in self.atom_types.items():
            lines += f"{itype:8d} {atom.mass:10.3f}  # {atom.element}\n"

        lines += '\n Atoms \n\n'
        # atom style is charge
        # atom-ID atom-type q x y z
        total_charge = 0.
        for iat, atom in enumerate(self.atoms):
            x, y, z = self.structure.cart_coords[iat]
            lines += (f"{iat + 1:6d} {atom.itype:6d} "
                      f"{atom.charge:10.4f} {x:12.6f} {y:12.6f} {z:12.6f}\n")
            total_charge += atom.charge
        print(f"total charge = {total_charge}")

        lines += "\n"

        if filename:
            with open(filename, "w", encoding="utf8") as f:
                f.write(lines)
        else:
            return lines

    @classmethod
    def from_file(cls, filename: Union[str, TextIO], elements: list[str]):
        """ Read data from a lammps data file and return the object

        Args:
            filename (str or TextIO): file path or file object of the data file
            elements (list): list of elements (list of string)
        """

        # parse file
        try:
            # try to open file
            with open(filename, "r", encoding="utf-8") as fout:
                header, attypes, coords, charges = self._parse_data_file(fout)
        except TypeError:
            # parse the file object
            header, attypes, coords, charges = self._parse_data_file(filename)

        atoms = list()
        atom_types = dict()
        masses = header["masses"]
        for iat, attype in enumerate(attypes):
            atom = Atom(attype, mass=[attype], element=elements[attype - 1],
                        charge=charges[iat])
            atoms.append(atom)

            if attype not in atom_types:
                atom_types[attype] = atom

        # lattice is supposed to be orthorhombic
        lattice = Lattice.orthorhombic(xhi - xlo, yhi - ylo, zhi - zlo)
        structure = Structure(
            lattice,
            [atom.element for atom in atoms],
            coords,
            coords_are_cartesian=True,
        )

        return cls(structure, atoms, atom_types, title=header["title"])

    @staticmethod
    def _parse_data_file(fdata: TextIO):
        """ parse the data file in argument and returns the sections in
        the data file """

        title = fdata.readline()
        fdata.readline()

        # read header
        masses = dict()
        for line in fdata:
            if m := re.match(r"^\s*(\d+)\s+atoms", line):
                n_atoms = int(m.group(1))
            elif m := re.match(r"^\s*(\d+)\s+atom types", line):
                n_types = int(m.group(1))
            elif m := re.match(r"^\s*([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+xlo\s+xhi", line):
                xlo, xhi = [float(val) for val in m.groups()]
            elif m := re.match(r"^\s*([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+ylo\s+yhi", line):
                ylo, yhi = [float(val) for val in m.groups()]
            elif m := re.match(r"^\s*([+-]?\d+\.\d+)\s+([+-]?\d+\.\d+)\s+zlo\s+zhi", line):
                zlo, zhi = [float(val) for val in m.groups()]

            elif "Masses" in line:
                fdata.readline()
                while m := re.match(r"\s+(\d+)\s+(\d+\.\d+)", fdata.readline()):
                    itype = int(m.group(1))
                    mass = float(m.group(2))
                    masses[itype] = mass

            elif "Atoms" in line:
                break

        header_data = dict(
            title=title,
            atoms=n_atoms, atom_tyes=n_types,
            xlo=xlo, xhi=xhi, ylo=xlo, yhi=xhi, zlo=xlo, zhi=xhi,
            masses=masses
        )

        # suppose atom_style is charge
        coords = list()
        charges = list()
        attypes = list()

        fdata.readline()
        for iat in range(n_atoms):
            line = fdata.readline().split()
            charges.append(line[2])
            coords.append(line[3:6])
            attypes.append(line[1])

        coords = np.array(coords, dtype=np.float64)
        charges = np.array(charges, dtype=np.float64)
        attypes = np.array(attypes, dtype=np.int32)

        return header, attypes, coords, charges
