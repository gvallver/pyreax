#!/usr/bin/env python

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import pandas as pd
from pymatgen.core import Molecule


rea = list()
with open('sucess/reaSnipe.txt', 'r' ) as frea:
    for line in frea :
        sep = line
	#.split()
        name = sep
        dico_d = {name: '1.0'}
        rea.append(dico_d)
    

reapd = pd.DataFrame(rea)
plothist = reapd.count()
plothist = ((plothist.sort_values(ascending=False)) /  (plothist.sort_values(ascending=False).sum())*100).plot(kind='bar') 
#plothist.sort_values(ascending=False).plot(kind="bar")
plt.savefig('hist.png', bbox_inches='tight')


