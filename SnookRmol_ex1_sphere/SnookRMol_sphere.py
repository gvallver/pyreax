#!/usr/bin/env python

import sys
import os
import lammps
import pandas as pd
import numpy as np
sys.path.append("../src/")
from snooker import lammps_inputs
from snooker import utils
from pyreax import reax_species
from pyreax.reax_bo import ReaxBO
from pymatgen.core import Molecule
from scipy.constants import Avogadro as N_A
from scipy.constants import Boltzmann as k_B
from spheric_grid import SphericGrid


#Main tunable parameters
n_points = 10
T=500
name_molecule = "BzCl.xyz"
radius = 8.3 

mol = Molecule.from_file("../data_molecules/"+name_molecule)
M_OH = 17.0 * 1e-3 # kg.mol-1
M_O = 15.999 * 1e-3  # kg.mol-1
M_H = 1.008 * 1e-3  # kg.mol-1


# Vector O

v1O = np.sqrt(3 * k_B * T / (M_O / N_A))
dec1 = str(v1O* 1e10 * 1e-15)

#number of decimal for later velocity generation
count=0
for c in dec1 :
    if c == '0' :
        count+=1
    if c == '.' :
        continue
    else :
        pass

cent = str(1)    
for x in range(count+1):
    cent += str(0)
    
decO = 1/float(cent)

#VECTOR H

v2H = np.sqrt(3 * k_B * T / (M_H / N_A))
dec2 = str(v2H* 1e10 * 1e-15)

#number of decimal
count=0
for c in dec2 :
    if c == '0' :
        count+=1
    if c == '.' :
        continue
    else :
        pass
    
cent = str(1)    
for x in range(count+1):
    cent += str(0)
    
decH = 1/float(cent)

# AJOUT DE OH

species = [specie.symbol for specie in mol.species]
species += ["O", "H"]


#### Placer la molécule au centre de la sphère, attention ici tout les atomes sont utilises pour détermine le centre
# de gravité

spe = len(mol.cart_coords)
G = mol.cart_coords[0:spe].sum(axis=0) / spe
coord = mol.cart_coords - G

#### DEterminer les points de la surface de la sphère
distances=mol.distance_matrix
#radius = (8 + np.max(distances)) / 2

### type of sphere
grid1 = SphericGrid(n_points=n_points, radius=radius,grid_type="random")

x, y , z = grid1.unpack

points_data = " files containing all the initial points \n"
for cooordinates in range(len(x)) :
    points_data += f" {x[cooordinates]} {y[cooordinates]} {z[cooordinates]} \n"
    
with open("points.data", "w") as fdata:
    fdata.write(points_data)


## Result directory
path = os.getcwd()
# Check whether the specified path exists or not
isExist = os.path.exists(path+'/sucess')
if not isExist:  
  # Create a new directory because it does not exist 
  os.makedirs(path+'/sucess')


#INITIAL CONDITIONS
bond_OH = 1.  # A distance O-H
points=grid1.unpack.transpose()
list_rea_data = list()
list_point_rea = list()
numb = 0
count = 0


for i, point in enumerate(points, 1):
    
    #Usage of the grid to generate the position of OH + adding them to the coordinates
    rnd = np.random.random(3)
    rnd /= np.linalg.norm(rnd)
    H = point + bond_OH * rnd
    coords = np.concatenate([coord, point[np.newaxis], H[np.newaxis]])    
    #print(coords)
    
    #Updating the velocities adding the velocities of OH
    
    vel = utils.gen_velocities(mol) ## Velocities molecules, Default 298.15K
    
    u = - point.copy()   ## The opposite point on the spehre coordinate for the direction
    u /= np.linalg.norm(u)  ## norm of the OH vector velocity
    vH = np.random.normal(v2H* 1e10 * 1e-15, decH) * u  # A.fs-1
    vO = np.random.normal(v1O* 1e10 * 1e-15, decO) * u  # A.fs-1

    vels = np.concatenate([vel, vO[np.newaxis], vH[np.newaxis]])
    #print(vels)
    
    xyz_str = utils.xyz_file(species, coords)
    molup = Molecule.from_str(xyz_str, fmt='xyz')
    
    #FABRICATION DU DATAFILES + L ORDRE DS SPECIES POUR LE INPUT FILE
    data_file, atom_types = lammps_inputs.data_file(molup, velocities=vels, box_size = 15.0)
    el = list()
    
    for itype, element in enumerate(atom_types, 1):
        el.append(str(element))

    with open("file.data", "w") as fdata:
        fdata.write(data_file)
        
    # FABRICATION DE L INPUT FILE
        
    count+=1
    print(count)        
    inp = lammps_inputs.input_file(
    "../ffield/ffield.CHONCl-2022_weak", elements=el, n_step=30000, data_file="file.data")  ###dump_trj=100 Gives the force field location / the order element, n_step and the name of the data file

   # with open("run.inp", "w") as finp:
   #     finp.write(inp)

    
    try :

        lmp = lammps.lammps(cmdargs=["-log", "none", "-screen", "none",  "-nocite"])
        lmp.commands_string(inp)  
        lmp.close()

        #VERIF USING BO

        rbo = ReaxBO('bonds.dat', 'file.data', elements=el)
        rbo.atom_data[rbo.atom_data["step"] == 1]

        #DATAFRAME INITIAL

        df1 = rbo.atom_data[rbo.atom_data["step"] == 1]
        liste_iat = df1['iat'].to_list() 
        df1= df1.set_index('iat')

        #DATAFRAME FINAL

        df2 = rbo.atom_data[rbo.atom_data["step"] == 30000]
        df2= df2.set_index('iat')

        # LISTE OF CONNECITIVTY
        connectivity_diff = df1['nb'] - df2['nb']

        # PARAMERTERS INITIALISATION
        iat_rea = list()
        kind_of_rea = list()
        reaction = False
        str_name = str()
        z = 0
        i = 0
        sumi = sum(connectivity_diff)
        # Check of connectivity change otherwise Reaction = False
        for line in connectivity_diff :
            z += 1 
            if int(line) != 0 : 
                iat_rea.append(liste_iat[z-1])

                if int(line) == 1:
                    kind_of_rea.append('brk')

                if int(line) == -1:
                    kind_of_rea.append('form')

                reaction = True
            else :
                continue

        # If a reaction occur creates the name of the reaction

        if reaction == True:
            for el in iat_rea :  
                str_name +=str(species[el-1])+'-'+str(iat_rea[i])+'-'+str(kind_of_rea[i])+' '
                i += 1

        check_H_sharing = str_name.split()

        if sumi == -2 :
            if check_H_sharing[0][0] == 'H' :
                if check_H_sharing[1][0] == 'O' :
                    print('H-sharing')
                    reaction = False
              
        if reaction == True :  
            print('reaction'+str_name)
            numb += 1
            with open("sucess/clbz"+str(numb)+".data", "w") as fdata:
                fdata.write(data_file)  

            f2 = open("sucess/reaSnipe.txt", "a")
            f2.write(str_name+'\n')
            f2.close()

            f3 = open("sucess/reapoint.txt", "a")
            f3.write(str(point)+'\n')
            f3.close()    
            print('reaction')
            
    except :       
        print("Unexpected error")    
    
    







