#!/usr/bin/env python

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import pandas as pd
from pymatgen.core import Molecule


rea = list()
with open('sucess/reaSnipe.txt', 'r' ) as frea:
    for line in frea :
        sep = line
	#.split()
        name = sep
        dico_d = {name: '1.0'}
        rea.append(dico_d)
    

reapd = pd.DataFrame(rea)

plothist = reapd.count()
plothist.sort_values(ascending=False).plot(kind="bar")
plt.savefig('hist.png', bbox_inches='tight')

name_molecule = "BzCl.xyz"
mol = Molecule.from_file("../data_molecules/"+name_molecule)
spe = len(mol.cart_coords)
G = mol.cart_coords[0:spe].sum(axis=0) / spe
coord = mol.cart_coords - G
def column(matrix, i):
    return [row[i] for row in matrix]

x_mol = column(coord, 0)
y_mol = column(coord, 1)
z_mol = column(coord, 2)

x_point = list()
y_point = list()
z_point = list()
with open('points.data', 'r' ) as fpoint:
    fpoint.readline()
    for line in fpoint :
        div = line.split()
        x_point.append(float(div[0]))
        y_point.append(float(div[1]))
        z_point.append(float(div[2]))
    
x_ini = list()
y_ini = list()
z_ini = list()
point_rea = list()
with open('sucess/reapoint.txt', 'r' ) as fpointrea:
    fpointrea.readline()
    for line in fpointrea :
        div = line.split() 
        
        if div[0] == '[':
            div.pop(0)            
            x_ini.append(float(div[0]))
            y_ini.append(float(div[1]))
            z_ini.append(float(div[2][:-1]))
            
        else :
            x_ini.append(float(div[0][1:]))
            y_ini.append(float(div[1]))
            z_ini.append(float(div[2][:-1]))



fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

x_mol = np.array(x_mol)
y_mol = np.array(y_mol)
z_mol = np.array(z_mol)
x_ini = np.array(x_ini)
y_ini = np.array(y_ini)
z_ini = np.array(z_ini)
x_point = np.array(x_point)
y_point = np.array(y_point)
z_point = np.array(z_point)


ax.scatter(x_point, y_point, -z_point, zdir='z', c='orange', depthshade=False, s=1., marker=',')
ax.scatter(x_ini, y_ini, -z_ini, zdir='z', c='green', depthshade=False, s=10., marker='o')
ax.scatter(x_mol, y_mol, -z_mol, zdir='z', c='blue', depthshade=False, s=20., marker=',')


def rotate(angle):
    ax.view_init(azim=angle)

print("Making animation")
rot_animation = animation.FuncAnimation(fig, rotate, frames=np.arange(0, 362, 2), interval=100)
rot_animation.save('3Dimage.gif', dpi=300, writer='imagemagick')        

