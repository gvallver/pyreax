# pyreax

Python classes to manage input/output of a reaxFF simulations and data 
exploration of reaxFF parameters.

## TODO

* Look at `energy` module. Check why numerous parameters are `None`
* inculude `mk_trainset.py` to manage trainset. `/Users/gvallver/dev/lammps/compute/matthieu`
* include a notebook or examples that performs a complete reaxFF calculation

## Installation

Short installation with conda for only using the library:

```
$> conda create --name reax python=3.9 numpy pandas matplotlib jupyter
$> conda activate reax
$> python - m pip install git+https://git.univ-pau.fr/gvallver/pyreax.git
```

Short installation in developer mode by downloading all files:

```
$> conda create --name reax python=3.9 numpy pandas matplotlib jupyter
$> conda activate reax
$> git clone https://git.univ-pau.fr/gvallver/pyreax.git
$> cd pyreax
$> python -m pip install -e .
```

If you want to install additional tools for developer you may consider
the following:

```
$> python -m pip install -e .[DEV]
```

## Usage

The [Notebook]() folder contains several notebooks to illustrate how
to use the provided classes or functions.

## Authors and acknowledgment

* Matthieu Wolf
* Germain Salvato Vallverdu

## License

The project is distributed under the MIT license. It is as is and
without any warranty.
